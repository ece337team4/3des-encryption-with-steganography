# Step 1:  Read in the source file
analyze -format sverilog -lib WORK {expansion_dbox.sv straight_dbox.sv swapper.sv xor_32bit.sv xor_48bit.sv sbox_toplevel.sv sbox1.sv sbox2.sv sbox3.sv sbox4.sv sbox5.sv sbox6.sv sbox7.sv sbox8.sv des_round_toplevel.sv keygenperm.sv toplevelkey.sv compresskey.sv initial_permutation.sv final_permutation.sv split_bits.sv controller.sv master.sv slave.sv input_buffer.sv output_buffer.sv encryption_toplevel.sv triple_des_toplevel.sv steganography.sv flex_counter.sv stripbits.sv ULTIMATE_TOPLEVEL.sv}
elaborate ULTIMATE_TOPLEVEL -lib WORK
uniquify
# Step 2: Set design constraints
# Uncomment below to set timing, area, power, etc. constraints
# set_max_delay <delay> -from "<input>" -to "<output>"
# set_max_area <area>
# set_max_total_power <power> mW


# Step 3: Compile the design
compile -map_effort medium

# Step 4: Output reports
report_timing -path full -delay max -max_paths 1 -nworst 1 > reports/ULTIMATE_TOPLEVEL.rep
report_area >> reports/ULTIMATE_TOPLEVEL.rep
report_power -hier >> reports/ULTIMATE_TOPLEVEL.rep

# Step 5: Output final VHDL and Verilog files
write_file -format verilog -hierarchy -output "mapped/ULTIMATE_TOPLEVEL.v"
echo "\nScript Done\n"
echo "\nChecking Design\n"
check_design
quit
