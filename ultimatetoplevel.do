onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/tb_clk
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/tb_n_rst
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/DUT/DUT/curstate
add wave -noupdate -radix decimal /tb_ULTIMATE_TOPLEVEL/tb_M_HADDR
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/tb_M_HRDATA
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/tb_M_HWRITE
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/tb_M_HWDATA
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/tb_S_HWDATA
add wave -noupdate -radix decimal /tb_ULTIMATE_TOPLEVEL/tb_S_HADDR
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/tb_S_HWRITE
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/tb_WE
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/tb_RE
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/DUT/CUT/m_data
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/DUT/PUT/data_o
add wave -noupdate -radix decimal /tb_ULTIMATE_TOPLEVEL/DUT/PUT/output_data_select
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/DUT/DUT/data_encrypted
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/DUT/FUT/enc_key
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/DUT/WUT/stripped_m_data
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/DUT/WUT/c_state
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/DUT/WUT/new_m_data
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/DUT/CUT/i_data
add wave -noupdate -radix decimal /tb_ULTIMATE_TOPLEVEL/DUT/CUT/input_data_select
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/tb_WE
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/tb_RE
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/DUT/WUT/des1/roundkey_connect
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/DUT/WUT/cipher_out
add wave -noupdate -radix hexadecimal /tb_ULTIMATE_TOPLEVEL/DUT/WUT/des1/cipher_text
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/DUT/WUT/new_m_data
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/DUT/WUT/encrypt
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/DUT/WUT/des1/stagenum
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/DUT/WUT/des1/DUT1/sendready
add wave -noupdate /tb_ULTIMATE_TOPLEVEL/DUT/WUT/encrypt_ctrl
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {913247 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 219
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1941330 ps} {2132066 ps}
