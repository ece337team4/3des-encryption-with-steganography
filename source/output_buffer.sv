// $Id: Output Buffer
// File name:   output_buffer.sv
// Created:     4/16/2016
// Author:      Anirudh Ghantasala
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: output buffer where the master will write data from

module output_buffer(
    input wire clk,
    input wire n_rst,
    input wire [511:0] steg_img,
    input wire [3:0] output_data_select,
    input reg [63:0] decrypt_m,
    input wire send_data,
    output wire new_data,
    output reg [63:0] data_o
		     );
   

    reg [575:0] out_buf;
    reg [63:0] next_data_o;
    
    always_ff @ (posedge clk, negedge n_rst)
    begin
        if(~n_rst)
            data_o <= 0;
        else
            data_o <= next_data_o;
    end
    
    // output only the range selected by output_Data_select
    always_comb begin: store_values
       next_data_o = data_o;
       
       if(send_data)
        case(output_data_select)
        4'b0000: next_data_o = out_buf[63:0];
        4'b0001: next_data_o = out_buf[127:64];
        4'b0010: next_data_o = out_buf[191:128];
        4'b0011: next_data_o = out_buf[255:192];
        4'b0100: next_data_o = out_buf[319:256];
        4'b0101: next_data_o = out_buf[383:320];
        4'b0110: next_data_o = out_buf[447:384];
        4'b0111: next_data_o = out_buf[511:448];
        4'b1000: next_data_o = out_buf[575:512];
        default: next_data_o = out_buf[63:0];  // when in_data_select > 9
        endcase
    else
        next_data_o = out_buf[63:0];
    end
    
    // values in out_buf keep changing, data_o gets updated only on posedge clk
   assign out_buf[63:0]    = decrypt_m;  
   assign out_buf[575:64]  = steg_img;
    
    
    
    
    
    
     
endmodule

    
