// $Id: $
// File name:   expansion_dbox.sv
// Created:     4/18/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: DES Fx expansion DBOX

module expansion_dbox
  (
   input wire [31:0] expansion_in,
   output reg [47:0] expansion_out

   );


   always @ (expansion_in)
     begin
	expansion_out[47] = expansion_in[00];
	expansion_out[46] = expansion_in[31];
	expansion_out[45] = expansion_in[30];
	expansion_out[44] = expansion_in[29];
	expansion_out[43] = expansion_in[28];
	expansion_out[42] = expansion_in[27];
	expansion_out[41] = expansion_in[28];
	expansion_out[40] = expansion_in[27];
	expansion_out[39] = expansion_in[26];
	expansion_out[38] = expansion_in[25];
	expansion_out[37] = expansion_in[24];
	expansion_out[36] = expansion_in[23];
	expansion_out[35] = expansion_in[24];
	expansion_out[34] = expansion_in[23];
	expansion_out[33] = expansion_in[22];
	expansion_out[32] = expansion_in[21];
	expansion_out[31] = expansion_in[20];
	expansion_out[30] = expansion_in[19];
	expansion_out[29] = expansion_in[20];
	expansion_out[28] = expansion_in[19];
	expansion_out[27] = expansion_in[18];
	expansion_out[26] = expansion_in[17];
	expansion_out[25] = expansion_in[16];
	expansion_out[24] = expansion_in[15];
	expansion_out[23] = expansion_in[16];
	expansion_out[22] = expansion_in[15];
	expansion_out[21] = expansion_in[14];
	expansion_out[20] = expansion_in[13];
	expansion_out[19] = expansion_in[12];
	expansion_out[18] = expansion_in[11];
	expansion_out[17] = expansion_in[12];
	expansion_out[16] = expansion_in[11];
	expansion_out[15] = expansion_in[10];
	expansion_out[14] = expansion_in[09];
	expansion_out[13] = expansion_in[08];
	expansion_out[12] = expansion_in[07];
	expansion_out[11] = expansion_in[08];
	expansion_out[10] = expansion_in[07];
	expansion_out[09] = expansion_in[06];
	expansion_out[08] = expansion_in[05];
	expansion_out[07] = expansion_in[04];
	expansion_out[06] = expansion_in[03];
	expansion_out[05] = expansion_in[04];
	expansion_out[04] = expansion_in[03];
	expansion_out[03] = expansion_in[02];
	expansion_out[02] = expansion_in[01];
	expansion_out[01] = expansion_in[00];
	expansion_out[00] = expansion_in[31];
	


     end
endmodule // expansion_dbox
