// $Id: $
// File name:   xor_48bit.sv
// Created:     4/18/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: DES function xor with round Key


module xor_48bit
  (
   input wire [47:0] plaintext_in,
   input wire [47:0] roundkey_in,
   output reg [47:0] xor_out
   );

   assign xor_out = (plaintext_in ^ roundkey_in);
   
endmodule // xor_48bit

