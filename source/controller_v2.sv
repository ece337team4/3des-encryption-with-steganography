ntroller
// File name:   controller.sv
// Created:     4/16/2016
// Author:      Ayush Malhotra
// Lab Section: 337-01
// Version:     2.0  Initial Design Entry
// Description: controller fsm, the brains of the entire program

module controller(
input wire clk,
	input wire n_rst,
	input reg [18:0]start_addr,
	input reg encryption,
	input reg data_ready,
	input reg start,
	//input reg new_data,
	input reg data_encrypted,
	input reg data_steg,
	input reg data_done,
	output reg [3:0]out_data_select,
	output reg e_re,
	output reg e_we,
	output reg transfer,
	output reg new_i_data,
	output reg [3:0]in_data_select,
	output reg encrypt,
	output reg new_m_data,
	output reg [18:0]addr_send
);

	reg [8:0]count_i, count_m, count_d;
	reg packet_done, packet_doon, packet_doone, shift_strobe_m, shift_strobe_i, shift_strobe_d;

		flex_counter #(9) emmy (.clk(clk), .n_rst(n_rst), .clear(packet_done), .count_enable(shift_strobe_m), 
			    .rollover_val(9'd33), .count_out(count_m), .rollover_flag(packet_done));
	flex_counter #(9) ayyey (.clk(clk), .n_rst(n_rst), .clear(packet_doon), .count_enable(shift_strobe_i), 
			    .rollover_val(9'd256), .count_out(count_i), .rollover_flag(packet_doon));
	flex_counter #(9) fyeye (.clk(clk), .n_rst(n_rst), .clear(packet_doone), .count_enable(shift_strobe_d), 
	
