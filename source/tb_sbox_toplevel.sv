// $Id: $
// File name:   tb_sbox_toplevel.sv
// Created:     4/20/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: test bench for sboxes


`timescale 10ns/100ps

module tb_sbox_toplevel
  ();

   wire [47:0] tb_in;
   wire [31:0] tb_out;
   reg [47:0]  tb_test_in;

   assign tb_in = tb_test_in;

   sbox_toplevel DUT (
		.in(tb_in),
		.out(tb_out)
		);
   initial begin
      
      tb_test_in = 48'b011000010001011110111010100001100110010100100111;
      
   
   end
endmodule // tb_xor_48_bit
