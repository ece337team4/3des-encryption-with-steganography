// $Id: $
// File name:   encryption_toplevel.sv
// Created:     4/24/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Top level file that coordinates DES encryption

module encryption_toplevel
  (
   input wire clk,
   input wire n_rst,
   input wire [63:0] plain_text,
   input wire [63:0] full_key,
   input wire start,
   input wire encrypt,
   input reg [1:0]stagenum, 
   output reg [63:0] cipher_text,
   output reg stage_done
  

   );
   reg [63:0] data1, n_data1;
   reg [63:0] data2;
   reg [63:0] finp;
   reg [63:0] data3; 	     
   reg 	      sendready;
   reg [47:0] roundkey_connect;
   reg [63:0] initialin;
   reg 	      noswap;
   
   toplevelkey DUT1(
		    .clk(clk),
		    .n_rst(n_rst),
		    .key(full_key),
		    .encrypt(encrypt),
		    .sendready(sendready),
		    .roundkey(roundkey_connect)
		    );
   initial_permutation DUT2(
			    .initial_perm_in(initialin),
			    .initial_perm_out(data3)
			    );
   final_permutation DUT3(
			  .final_perm_in(finp),
			  .final_perm_out(cipher_text)
			  );
   
   des_round_toplevel DUT4 (
			    .noswap(noswap),
			    .data_msg(data1),
			    .cipher_out(data2),
			    .round_key(roundkey_connect)
			    );
   
   
   typedef enum logic [4:0] {idle, init_perm, final_perm, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16} state_type;

   state_type c_state, n_state;

   always_ff @ (posedge clk, negedge n_rst) begin
      if(n_rst == 0) begin
	 c_state <= idle;
	 data1 <= 0;
      end
      else begin
	 c_state <= n_state;
	 data1 <= n_data1;
      end
   end



   always_comb begin
      n_state = c_state;
      n_data1 = data1;
      sendready = 0;
      stage_done = 0;
      noswap = 0;
      case(c_state)
	idle: begin
	   if(start == 1) begin
	      n_state = init_perm;
	      sendready = 0;
  
	   end
	   else begin
	      n_state = idle;
	   end
	   stage_done = 0;
	   noswap = 0;

	end

	init_perm: begin
      
	   if(stagenum == 2'd1 || stagenum == 2'd2 || stagenum == 2'd3)
	     sendready = 1;
	   else
	     sendready = 0;
	   
	   n_state = r1;
	   n_data1 = data3;
	   stage_done = 0;
	   noswap = 0;
	   
	end

	r1: begin
	  
	   n_state = r2;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;
	end
	
	r2: begin
	   n_state = r3;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r3: begin
	   n_state = r4;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r4: begin
	   n_state = r5;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r5: begin
	   n_state = r6;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r6: begin
	   n_state = r7;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r7: begin
	   n_state = r8;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r8: begin
	   n_state = r9;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r9: begin
	   n_state = r10;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r10: begin
	   n_state = r11;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r11: begin
	   n_state = r12;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r12: begin
	   n_state = r13;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r13: begin
	   n_state = r14;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r14: begin
	   n_state = r15;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r15: begin
	   n_state = r16;
	   n_data1 = data2;
	   sendready = 1;
	   noswap = 0;
	   stage_done = 0;

	end
	r16: begin
	   n_state = final_perm;
	   n_data1 = data2;	
	   sendready = 1;
	   noswap = 1;
	   stage_done = 0;
	end
	final_perm: begin
	   sendready = 0;
	   
	   n_state = idle;
	   //n_data1 = data2;
	   stage_done = 1;	  
	   noswap = 0;
	   
	end
      endcase // case (c_state)
      

   end // always_comb begin
   
   assign finp = (c_state == final_perm)? data1 : 64'b0;
   assign initialin = (c_state == init_perm)? plain_text : 64'b0;
   
   






endmodule // encryption_toplevel
