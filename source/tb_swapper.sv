// $Id: $
// File name:   tb_swapper.sv
// Created:     4/19/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: test bench for swapper file

`timescale 1ns/100ps


module tb_swapper
  ();   
   wire [63:0]tb_in;
   wire [63:0]tb_out;
   reg [63:0] tb_test_inputs;
   
   assign tb_in = tb_test_inputs;
   
   
   swapper DUT(
	    .in(tb_in),
	    .out(tb_out)
	    );   
   initial begin
      tb_test_inputs[63:0] = 64'b1111111111111111111111111111111100000000000000000000000000000000;
      
				    


   end
endmodule // tb_swapper