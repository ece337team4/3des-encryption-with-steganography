// $Id: $
// File name:   xor_32bit.sv
// Created:     4/19/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: 32 bit xor. Xor's left bits on plaintext with manipulated right 32 bits of plaintext input


module xor_32bit
  (
   input wire [31:0] left_in,
   input wire [31:0] right_in,
   output reg [31:0] xor_out
   );

   assign xor_out = (left_in ^ right_in);
   
endmodule // xor_32bit

