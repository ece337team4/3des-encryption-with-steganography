// $Id: $
// File name:   tb_stripbits.sv
// Created:     4/23/2016
// Author:      Akshay Sanjeev Daftardar
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: strip bits 
module tb_stripbits();
	reg [511:0] tb_image;
	reg [63:0] tb_message;
   stripbits STRIP( 
		    .image(tb_image), 
		    .message(tb_message)
		    );
	initial
	begin
		tb_image = 512'h11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;
	end
endmodule
		

