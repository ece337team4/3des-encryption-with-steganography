// $Id: $
// File name:   inital_permutation.sv
// Created:     4/16/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Initial Permutation table

module initial_permutation

  (
   input wire [63:0] initial_perm_in,
   output reg [63:0] initial_perm_out
   );

   always_comb
     begin

	initial_perm_out[63] = initial_perm_in[06];//58
	initial_perm_out[62] = initial_perm_in[14];//50
	initial_perm_out[61] = initial_perm_in[22];//42
	initial_perm_out[60] = initial_perm_in[30];//34
	initial_perm_out[59] = initial_perm_in[38];//26
	initial_perm_out[58] = initial_perm_in[46];//18
	initial_perm_out[57] = initial_perm_in[54];//10
	initial_perm_out[56] = initial_perm_in[62];//02
	initial_perm_out[55] = initial_perm_in[04];//60
	initial_perm_out[54] = initial_perm_in[12];//52
	initial_perm_out[53] = initial_perm_in[20];//44
	initial_perm_out[52] = initial_perm_in[28];//36
	initial_perm_out[51] = initial_perm_in[36];//28
	initial_perm_out[50] = initial_perm_in[44];//20
	initial_perm_out[49] = initial_perm_in[52];//12
	initial_perm_out[48] = initial_perm_in[60];//04
	initial_perm_out[47] = initial_perm_in[02];//62
	initial_perm_out[46] = initial_perm_in[10];//54
	initial_perm_out[45] = initial_perm_in[18];//46
	initial_perm_out[44] = initial_perm_in[26];//38
	initial_perm_out[43] = initial_perm_in[34];//30
	initial_perm_out[42] = initial_perm_in[42];//22
	initial_perm_out[41] = initial_perm_in[50];//14
	initial_perm_out[40] = initial_perm_in[58];//06
	initial_perm_out[39] = initial_perm_in[00];//64
	initial_perm_out[38] = initial_perm_in[08];//56
	initial_perm_out[37] = initial_perm_in[16];//48
	initial_perm_out[36] = initial_perm_in[24];//40
	initial_perm_out[35] = initial_perm_in[32];//32
	initial_perm_out[34] = initial_perm_in[40];//24
	initial_perm_out[33] = initial_perm_in[48];//16
	initial_perm_out[32] = initial_perm_in[56];//08
	initial_perm_out[31] = initial_perm_in[07];//57
	initial_perm_out[30] = initial_perm_in[15];//49
	initial_perm_out[29] = initial_perm_in[23];//41
	initial_perm_out[28] = initial_perm_in[31];//33
	initial_perm_out[27] = initial_perm_in[39];//25
	initial_perm_out[26] = initial_perm_in[47];//17
	initial_perm_out[25] = initial_perm_in[55];//09
	initial_perm_out[24] = initial_perm_in[63];//01
	initial_perm_out[23] = initial_perm_in[05];//59
	initial_perm_out[22] = initial_perm_in[13];//51
	initial_perm_out[21] = initial_perm_in[21];//43
	initial_perm_out[20] = initial_perm_in[29];//35
	initial_perm_out[19] = initial_perm_in[37];//27
	initial_perm_out[18] = initial_perm_in[45];//19
	initial_perm_out[17] = initial_perm_in[53];//11
	initial_perm_out[16] = initial_perm_in[61];//03
	initial_perm_out[15] = initial_perm_in[03];//61
	initial_perm_out[14] = initial_perm_in[11];//53
	initial_perm_out[13] = initial_perm_in[19];//45
	initial_perm_out[12] = initial_perm_in[27];//37
	initial_perm_out[11] = initial_perm_in[35];//29
	initial_perm_out[10] = initial_perm_in[43];//21
	initial_perm_out[09] = initial_perm_in[51];//13
	initial_perm_out[08] = initial_perm_in[59];//05
	initial_perm_out[07] = initial_perm_in[01];//63
	initial_perm_out[06] = initial_perm_in[09];//55
	initial_perm_out[05] = initial_perm_in[17];//47
	initial_perm_out[04] = initial_perm_in[25];//39
	initial_perm_out[03] = initial_perm_in[33];//31
	initial_perm_out[02] = initial_perm_in[41];//23
	initial_perm_out[01] = initial_perm_in[49];//15
	initial_perm_out[00] = initial_perm_in[57];//07

     end // always @ (initial_perm_in)
endmodule // initial_permutation
