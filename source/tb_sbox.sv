// $Id: $
// File name:   tb_sbox.sv
// Created:     4/16/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: SBOX Test Bench

`timescale 1ns/100ps


module tb_sbox
  ();   
   wire [47:0]tb_in;
   wire [31:0]tb_out;
   reg [47:0] tb_test_inputs;
   integer    i;
   
   assign tb_in = tb_test_inputs;
   
   
   sbox DUT(
	    .in(tb_in),
	    .out(tb_out)
	    );   
   initial begin
      tb_test_inputs[41:0] = 42'b0000;
      
      tb_test_inputs[47:42] = 6'b000000;
      for(i = 0; i <=64; i = i + 1) begin
	 #1
	 tb_test_inputs = tb_test_inputs + 1;
	 
      end
      
				    


   end
endmodule // tb_sbox
