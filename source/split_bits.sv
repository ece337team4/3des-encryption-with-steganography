// $Id: $
// File name:   split_bits.sv
// Created:     4/24/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Splits input plaintext into left and right 32 chunks

module split_bits(
		  input wire [63:0] plaintext,
		  output reg [31:0] bitsL,
		  output reg [31:0] bitsR
		  );

   always_comb
     begin
	     bitsL = plaintext[63:32];
	     bitsR = plaintext[31:0];
     end
   

endmodule // split_bits
