// $Id: Controller
// File name:   controller.sv
// Created:     4/16/2016
// Author:      Anirudh Ghantasala
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: controller fsm, the brains of the entire program


module controller(
		  input wire 	    clk,
		  input wire 	    n_rst,
		  input reg [18:0]  start_addr,
		  input reg 	    encryption,
		  input reg 	    data_ready,
		  input reg 	    start,
		  //input reg new_data,
		  input reg 	    data_encrypted,
		  input reg 	    data_steg,
		  input reg 	    data_done,
		  output reg [3:0]  out_data_select,
		  output reg 	    e_re,
		  output reg 	    e_we,
		  output reg 	    transfer,
		  output reg 	    new_i_data,
		  output reg [3:0]  in_data_select,
		  output reg 	    encrypt,
		  output reg 	    new_m_data,
		  output reg [18:0] addr_send,
		  output reg 	    send_d
		  );
   
   reg [8:0] 				count_i, count_m, count_d;
   reg 					packet_done, packet_doon, packet_doone, shift_strobe_m, shift_strobe_i, shift_strobe_d;
   
   // count up to keep track of which address we are reading from (image address and message address)
   flex_counter #(9) emmy (.clk(clk), .n_rst(n_rst), .clear(packet_done), .count_enable(shift_strobe_m), 
			   .rollover_val(9'd33), .count_out(count_m), .rollover_flag(packet_done));
   flex_counter #(9) ayyey (.clk(clk), .n_rst(n_rst), .clear(packet_doon), .count_enable(shift_strobe_i), 
			    .rollover_val(9'd256), .count_out(count_i), .rollover_flag(packet_doon));
   /*flex_counter #(9) fyeye (.clk(clk), .n_rst(n_rst), .clear(packet_doone), .count_enable(shift_strobe_d), 
			    .rollover_val(9'd33), .count_out(count_d), .rollover_flag(packet_doone));*/
   
   
   reg [18:0] 				addr_m, addr_i;      // ASSUMPTION : EACH ADDRESS UNIT = 1 BYTE (img starts 2048 bits after message)

   
   
   typedef enum 			logic [5:0] {IDLE, R1, WAIT8, IMGDONE, ENCRYPT, RIMG1, RIMG2, RIMG3, RIMG4, RIMG5, RIMG6, RIMG7, RIMG8, JWAIT1, STORE, STEG, WAITSTEG, WIMG1, RWAIT1, WIMG2, RWAIT2, WIMG3, RWAIT3, WIMG4, RWAIT4, WIMG5, RWAIT5, WIMG6, RWAIT6, WIMG7, RWAIT7, WIMG8, RWAIT8, RD1, RD2, RD3, RD4, RD5, RD6, RD7, RD8, STRIP, STRIP2, STRIP3, OUT1, OUT2} state_type;

   state_type curstate, nextstate;

   always_ff @(posedge clk, negedge n_rst)
     begin
	if(~n_rst)
	  curstate <= IDLE;
	else
	  curstate <= nextstate;
     end

   always_comb begin: CONTROLLER_FSM
      nextstate = curstate;
      addr_m = start_addr;
      addr_i = start_addr + 9'd256;
      e_re = 0;
      e_we = 0;
      transfer = 0;
      out_data_select = 0;
      new_m_data = 0;
      send_d = 0;
		encrypt = 0;
      new_i_data = 0;
		addr_send = 0;
		in_data_select = 4'd10;

   case(curstate)
	IDLE:										// IDLE
	  begin
	     if(start && encryption)
	     begin
		  		nextstate = R1;
		  		addr_send = addr_m + count_m*8;
		  		e_re = 1;
		  		transfer = 1; 																																						 // anirudh changed
	     end	
	     else if(start == 1'b1 && ~encryption)			// go to decrypt block
	       nextstate = RD1;
	     else
	       nextstate = IDLE;
	  end
	R1:		
	  begin									// read in next message chunk of 64 bits
	     nextstate = JWAIT1;
		  addr_send = 0;
	  end		
	JWAIT1:
	  begin
	     nextstate = ENCRYPT;
	     in_data_select = 4'd0;
		  addr_send = 0;  
   end  
	
	ENCRYPT:									// tell encrypt block that theres new message data
	  begin
	     new_m_data = 1;
	     encrypt = 1;
	     nextstate = RIMG1;
	     addr_send = 0;
	  end
	RIMG1:										// read in next image chunk of 64 bits
	  begin
	     e_re = 1;
	     transfer = 1;
	     addr_send = addr_i+(count_i*8);
	     nextstate = RIMG2;
	     new_m_data = 0;
	  end // case: RIMG1
	RIMG2:											// read in last 7 chunks
	  begin
	     e_re = 1;
	     transfer = 1;
	     addr_send = addr_i + count_i*8;
	     in_data_select = 4'd1;
	     new_m_data = 0;		
		  nextstate = RIMG3;
	  end 

	RIMG3:
	  begin
	     e_re = 1;
	     transfer = 1;
	     addr_send = addr_i + count_i*8;
	     in_data_select = 4'd2;
	     nextstate = RIMG4;
	     new_m_data = 0;
	  end // case: RIMG1

	RIMG4:
	  begin
	     e_re = 1;
	     transfer = 1;
	     addr_send = addr_i + count_i*8;
	     in_data_select = 4'd3;
	     
	     nextstate = RIMG5;
	     new_m_data = 0;
	  end // case: RIMG1

	RIMG5:
	  begin
	     e_re = 1;
	     transfer = 1;
	     addr_send = addr_i + count_i*8;
	     in_data_select = 4'd4;   
	     nextstate = RIMG6;
	     new_m_data = 0;
	  end // case: RIMG1

	RIMG6:
	  begin
	     e_re = 1;
	     transfer = 1;
	     addr_send = addr_i + count_i*8;
	     in_data_select = 4'd5;   
	     nextstate = RIMG7;
	     new_m_data = 0;
	  end // case: RIMG1

	RIMG7:
	  begin
	     e_re = 1;
	     transfer = 1;
	     addr_send = addr_i + count_i*8;
	     in_data_select = 4'd6;      
	     nextstate = RIMG8;
	     new_m_data = 0;
	  end // case: RIMG1

	RIMG8:
	  begin
	     e_re = 1;
	     transfer = 1;
	     addr_send = addr_i + count_i*8;
	     in_data_select = 4'd7;     
	     nextstate = IMGDONE;
	     new_m_data = 0;
	  end // case: RIMG8

	IMGDONE:
	  begin
	     nextstate = STORE;
	     in_data_select = 4'd8;
	  end  
	
	STORE:					// store data in the next set of 64 bits of inpt buffe
	  begin
	     if(data_encrypted) 			
	       nextstate = STEG;
	     else
	       nextstate = STORE;
	  end

	STEG:
	  begin
	     new_i_data = 1;		// start steg
	     nextstate = WAITSTEG;
	     /*	if(data_steg)
	      nextstate = WIMG1;
	      else
	      nextstate = STEG;*/
	  end

	WAITSTEG:
	  begin
	     nextstate = WIMG1;
	    // e_we = 1;
	    // addr_send = addr_i + (count_i-8)*8;
	  end  

	WIMG1:										// next 8 states write stegged image to ram
	  begin
	     e_we = 1;
	     out_data_select = 4'd1;
	     send_d = 1;
	     
	     addr_send = addr_i + (count_i-8)*8 ;
	     // of address.
	     transfer = 1;			
	     nextstate = WIMG2;
	  end

	WIMG2:
	  begin
	     e_we = 1;
	     addr_send = addr_i + (count_i-7)*8;
	     out_data_select = 4'd2;
	     send_d = 1;
	     
	     
	     //	next_addr_send = addr_i + (count_m-1)*64 + 8;	
	     transfer = 1;
	     nextstate = WIMG3;
	  end

	WIMG3:
	  begin
	     e_we = 1;
	     out_data_select = 4'd3;
	     send_d = 1;
	     
	     addr_send = addr_i + (count_i-6)*8;
	     
	     //	next_addr_send = addr_i + (count_m-1)*64 + 16;
	     transfer = 1;
	     nextstate = WIMG4;
	  end   

	WIMG4:
	  begin
	     e_we = 1;
	     out_data_select = 4'd4;
	     send_d = 1;
	     
	     addr_send = addr_i + (count_i-5)*8;
	     
	     //	next_addr_send = addr_i + (count_m-1)*64 + 24;
	     transfer = 1;
	     nextstate = WIMG5;
	     ;
	  end

	WIMG5:
	  begin
	     e_we = 1;
	     out_data_select = 4'd5;
	     send_d = 1;
	     
	     //	next_addr_send = addr_i + (count_m-1)*64 + 32;
	     addr_send = addr_i + (count_i-4)*8;
	     transfer = 1;
	     nextstate = WIMG6;
	  end

	WIMG6:
	  begin
	     e_we = 1;
	     send_d = 1;
	     
	     out_data_select = 4'd6;
	     addr_send = addr_i + (count_i-3)*8;
	     
	     //	next_addr_send = addr_i + (count_m-1)*64 + 40;
	     transfer = 1;
	     nextstate = WIMG7;
	  end
	
	WIMG7:
	  begin
	     e_we = 1;
	     send_d = 1;
	     
	     out_data_select = 4'd7;
	     addr_send = addr_i + (count_i-2)*8;
	     
	     //	next_addr_send = addr_i + (count_m-1)*64 + 48;
	     transfer = 1;
	     nextstate = WIMG8;
	  end
	
	WIMG8:												// loop process until 32 chunks of message have been read/processed
	  begin
	     e_we = 1;
	     send_d = 1;
	     
	     out_data_select = 4'd8;
	     addr_send = addr_i + (count_i-1)*8;
	     transfer = 1;
	     nextstate = WAIT8;
	  end
	WAIT8:
	  begin
         e_we = 0;
	    	transfer = 0;
	     	if(count_m != 32)
		  		nextstate = R1;
	     	else
	      	nextstate = IDLE;
	  end
	//decrypt --------------------------------------------------------------------------------------------------------
	RD1:
	  begin
	     e_re = 1;
	     transfer = 1;
		  addr_send = addr_i + count_i*8;
	     nextstate = RD2;
	  end
	RD2:
	  begin
	     e_re = 1;
	     transfer = 1;
	     in_data_select = 4'd1;			// image data comes in this cycle
		  addr_send = addr_i + count_i*8;
		  nextstate = RD3;
	  end
	RD3:
	  begin
	     e_re = 1;
	     transfer = 1;
	 	   in_data_select = 4'd2;
			addr_send = addr_i + count_i*8;
			nextstate = RD4;
	  end
	RD4:
	  begin
	     e_re = 1;
	     transfer = 1;
	     in_data_select = 4'd3;
		  addr_send = addr_i + count_i*8;		  
			nextstate = RD5;
	  end
	RD5:
	  begin
	     e_re = 1;
	     transfer = 1;
	     in_data_select = 4'd4;
		 addr_send = addr_i + count_i*8;
		  nextstate = RD6;
	  end
	RD6:
	  begin
	     e_re = 1;
	     transfer = 1;
		  addr_send = addr_i + count_i*8;
	     in_data_select = 4'd5;
		   nextstate = RD7;
	  end
	RD7:
	  begin
	     e_re = 1;
	     transfer = 1;
	     in_data_select = 4'd6;
		  addr_send = addr_i + count_i*8;
		   nextstate = RD8;
	  end
	RD8:
	  begin
	     e_re = 1;
	     transfer = 1;
	     in_data_select = 4'd7;
		  addr_send = addr_i + count_i*8;
		   nextstate = STRIP;
	  end
	STRIP:
	  begin
	     in_data_select = 4'd8;
	     nextstate = STRIP2;
	  end
	STRIP2:
	 begin
		 nextstate = STRIP3;
	 end
	STRIP3:
	begin
		 new_m_data = 1;
		  encrypt = 0;
		  
	     if(data_encrypted)
	       nextstate = OUT1;
		  else
	       nextstate = STRIP3;
	end
	OUT1:
	  begin
	     e_we = 1;
	     transfer = 1;
		  nextstate = OUT2;
	  end
	OUT2:
	  begin
	     out_data_select = 4'd1;			// output the decrypted message stored in bits [63:0] of output buffer
	     if(count_m == 9'd32)
	       nextstate = IDLE;
		  else
			 nextstate = RD1;
	  end
      endcase
   end



   // these strobe each time message or image data is read
   assign shift_strobe_m = (curstate == R1 || curstate == RD1);
   assign shift_strobe_i = (curstate == RIMG1 || curstate == RIMG2 || curstate == RIMG3 || curstate == RIMG4 || curstate == RIMG5 || curstate == RIMG6 || curstate == RIMG7 || curstate == RIMG8 ||
										curstate == RD1 || curstate == RD2 || curstate == RD3 || curstate == RD4 || curstate == RD5 || curstate == RD6 || curstate == RD7 || curstate == RD8);
   //assign shift_strobe_d = (curstate == RD1);


endmodule









