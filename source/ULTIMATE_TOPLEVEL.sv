// $Id: Controller
// File name:   ULTIMATE_TOPLEVEL.sv
// Created:     4/16/2016
// Author:      Anirudh Ghantasala
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Total file with all the wrappers

module ULTIMATE_TOPLEVEL(

			 //MASTER SIGNALS			 
			 input wire 	    clk,
			 input wire 	    n_rst,
			 input wire 	    M_HRESP,
			 input wire 	    M_HREADY,
			 input wire [63:0]  M_HRDATA,
			 output wire [63:0] M_HWDATA,
			 output wire [18:0] M_HADDR,
			 output wire [2:0]  M_HBURST,
			 output wire [2:0]  M_HSIZE,
			 output wire [3:0]  M_HPROT,
			 output wire 	    M_HMASTLOCK,
			 output wire [1:0]  M_HTRANS,
			 output reg 	    M_HWRITE,
			 

			 //SLAVE SIGNALS
			 input wire 	    S_hsel1,
			 input wire 	    S_HWRITE,
			 input wire [2:0]   S_HBURST,
			 input wire [3:0]   S_HPROT,
			 input wire [2:0]   S_HSIZE,
			 input wire 	    S_HREADY,
			 output wire 	    S_HREADYOUT,
			 output wire 	    S_HRESP,
			 input wire [63:0]  S_HWDATA,
			 input wire [1:0]   S_HTRANS,
			 input wire 	    S_HMASTLOCK,
			 input wire [18:0]  S_HADDR



			 );

   reg 					    enorden, data_ready, data_encrypted, data_steg, data_done, e_re, e_we, transfer, new_i_data, encrypt, new_m_data, start, transfer_complete;
   reg [3:0] 				    out_data_select, in_data_select;   
   reg [18:0] 				    start_addr, addr_send;
   reg [63:0] 				    m_data, data_o, encrypt_m, data_out, decrypt_m, S_hwdata_in, stripped_m_data;;			// MISTAKE WITH DECRYPT_M
   reg [191:0] 				    enc_key;
   reg [511:0] 				    steg_img, i_data;				
   
     master QUT (
	       .HRESP(M_HRESP), 
	       .HREADY(M_HREADY), 
	       .e_re(e_re), 
	       .e_we(e_we), 
	       .send_addr(addr_send), 
	       .transfer(transfer), 
	       .data_out(data_out), 
	       .HRDATA(M_HRDATA), 
	       .HWDATA(M_HWDATA), 
	       .HSIZE(M_HSIZE), 
	       .HWRITE(M_HWRITE), 
	       .HBURST(M_HBURST), 
	       .HPROT(M_HPROT), 
	       .HADDR(M_HADDR), 
	       .HMASTLOCK(M_HMASTLOCK), 
	       .HTRANS(M_HTRANS), 
	       .controller_hresp(controller_hresp), 
	       .data_in(data_o), 
	       .transfer_complete(transfer_complete)
	       );

   slave FUT(
	     .clk(clk), 
	     .n_rst(n_rst), 
	     .hsel1(S_hsel1), 
	     .hwrite(S_HWRITE), 
	     .hburst(S_HBURST), 
	     .hprot(S_HPROT), 
	     .hsize(S_HSIZE), 
	     .hmastlock2(S_HMASTLOCK), 
	     .htrans(S_HTRANS), 
	     .hready(S_HREADY), 
	     .hreadyout(S_HREADYOUT), 
	     .hresp(S_HRESP), 
	     .hwdata(S_HWDATA), 
	     .haddr(S_HADDR), 
	     .start(start), 
	     .enorde(enorden), 
	     .start_addr(start_addr),
	     .enc_key(enc_key)
	     );


   controller DUT (
		   .clk(clk), 
		   .n_rst(n_rst), 
		   .start_addr(start_addr), 
		   .encryption(enorden), 
		   .data_ready(data_ready), 
		   .data_encrypted (data_encrypted), 
		   .data_steg(data_steg), 
		   .data_done(data_done), 
		   .out_data_select(out_data_select), 
		   .e_re(e_re), 
		   .e_we(e_we), 
		   .transfer(transfer), 
		   .new_i_data(new_i_data), 
		   .in_data_select(in_data_select), 
		   .encrypt(encrypt), 
		   .new_m_data(new_m_data), 
		   .addr_send(addr_send), 
		   .start(start), 
		   .send_d(send_data)
		   );

   

  /* slavecontrol MUT(.clk(clk), .n_rst(n_rst), .hwdata_in(hwdata_in), .start_addr(start_addr), .enc_key(enc_key), .enorden(enorden), .start(start));*/

 


   input_buffer CUT (
		     .clk(clk), 
		     .n_rst(n_rst), 
		     .data_i(data_out), 
		     .input_data_select(in_data_select), 
		     .transfer_complete(transfer_complete), 
		     .data_ready(data_ready), 
		     .i_data(i_data), 
		     .m_data(m_data)
		     );

   output_buffer PUT (
		      .clk(clk), 
		      .n_rst(n_rst), 
		      .steg_img(steg_img), 
		      .output_data_select(out_data_select), 
		      .decrypt_m(encrypt_m), 
		      .send_data(send_data), 
		      .new_data(new_data), 
		      .data_o(data_o)
		      );

   //currmessage goes to decryption

   steganography NUT (
		      .encrypt_m(encrypt_m), 
		      .data_i(i_data), 
		      .inter(steg_img)
		      );

   triple_des_toplevel WUT (
			    .clk(clk), .n_rst(n_rst), 
			    .new_m_data(new_m_data), 
			    .encrypt(encrypt), 
			    .complete_key(enc_key), 
			    .m_data(m_data), 
			    .cipher_out(encrypt_m), 
			    .cipher_out_decrypt(decrypt_m), 
			    .data_encrypted (data_encrypted), 
			    .stripped_m_data(stripped_m_data)
			    );
   
   stripbits MUT(.image(i_data), .message(stripped_m_data));
   


endmodule




























