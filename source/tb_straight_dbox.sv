// $Id: $
// File name:   tb_straight_dbox.sv
// Created:     4/19/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Test bench for straight 32-bit permutation DES function

`timescale 10ns/100ps

module tb_straight_dbox
();

   wire [31:0] tb_in;
   wire [31:0] tb_out;
   reg [31:0]  tb_test_inputs;

   assign tb_in = tb_test_inputs;


   straight_dbox DUT (
		      .in(tb_in),
		      .out(tb_out)
		      );

   initial begin
      tb_test_inputs[31:0] = 32'b01011100100000101011010110010111;

   end
endmodule // tb_straight_dbox
