// $Id: $
// File name:   flex_counter.sv
// Created:     2/1/2016
// Author:      Anirudh Ghantasala
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: flexible and scalable counter with controlled rollover

module flex_counter
  #(
    parameter NUM_CNT_BITS = 4
)
  (
   input wire clk,
   input wire n_rst,
   input wire clear,
   input wire count_enable,
   input logic [NUM_CNT_BITS-1:0]rollover_val,
   output logic [NUM_CNT_BITS-1:0]count_out,
   output reg rollover_flag
   );

reg [NUM_CNT_BITS-1:0] next_count = '0; 
reg ROLL_NEXT;
  
always_ff @ (posedge clk, negedge n_rst) begin: RESET
    if (n_rst == 0)
      begin
        rollover_flag <= 0;
        count_out <= 0;
      end
    else
      begin
        count_out <= next_count;
        rollover_flag <= ROLL_NEXT;
      end
end // block: RESET
    
   
always_comb begin: COUNTLOGIC
   next_count = count_out;
   ROLL_NEXT = rollover_flag;
   
   if(clear)
      begin
        next_count = 0;
		ROLL_NEXT = 0;
      end
    else
      begin
	 if(count_enable)
	   begin
	      next_count = count_out+1;
	      ROLL_NEXT = 0;
	      
              if(rollover_val < next_count)
		next_count = 1;
	      if(next_count == rollover_val)
		ROLL_NEXT = 1;
           end
      end 

end // block: COUNTLOGIC
    

endmodule

	   
