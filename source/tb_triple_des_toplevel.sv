// $Id: $
// File name:   tb_encryption_toplevel.sv
// Created:     4/25/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Test bench for encryption toplevel
`timescale 1ns/100ps
module tb_triple_des_toplevel();
   reg clk;
   localparam CLK_PERIOD = 10ns;
   reg n_rst;
   
   reg [63:0] tb_m_data;
   reg [191:0] tb_complete_key;
   reg [63:0]  tb_cipher_out;
   reg 	       tb_new_m_data;
   reg 	       tb_encrypt_ctrl;
   reg 	       tb_encrypt;
   reg 	       tb_data_encrypted;
   
 	       
  	triple_des_toplevel HOHO(
				 .clk(clk), 
				 .n_rst(n_rst), 
				 .m_data(tb_m_data), 
				 .complete_key(tb_complete_key), 
				 .encrypt_ctrl(tb_encrypt_ctrl), 
				 .cipher_out(tb_cipher_out),
				 .encrypt(tb_encrypt),
				 .new_m_data(tb_new_m_data),
				 .data_encrypted(tb_data_encrypted)
				 );
	always
	begin
		clk = 1;
		#(CLK_PERIOD / 2);
		clk = 0;
		#(CLK_PERIOD / 2);
	end
	initial
	begin
		@(negedge clk);
		n_rst = 0;
		@(negedge clk);
		n_rst = 1;
		tb_encrypt = 0;
		tb_new_m_data = 0;
		tb_complete_key = 192'h3b3898371520f75e922fb510c71f436e3b3898371520f75e;
	        //tb_complete_key = 192'h3b3898371520f75e3b3898371520f75e3b3898371520f75e;
	   
	        tb_m_data = 64'habcde12345abcde6;
		@(negedge clk);

		tb_new_m_data = 1;
		@(negedge clk);
		tb_new_m_data = 0;
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
		@(negedge clk);
	   tb_new_m_data = 1;
	   
		
	end
endmodule
	
		
