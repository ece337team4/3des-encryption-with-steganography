// $Id: $
// File name:   tb_expansion_dbox.sv
// Created:     4/18/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Test bench for expansion DBOX


`timescale 1ns/100ps

module tb_expansion_dbox
  ();
   wire [31:0] tb_in;
   wire [47:0] tb_out;

   reg [31:0]  tb_test_inputs;

   assign tb_in = tb_test_inputs;
   expansion_dbox DUT(
		      .expansion_in(tb_in),
		      .expansion_out(tb_out)
		      );
   
   initial begin
      tb_test_inputs = 32'b11110000101010101111000010101010;
      

   end
endmodule // tb_expansion_dbox
