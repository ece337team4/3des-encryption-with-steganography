// $Id: Input Buffer
// File name:   controller.sv
// Created:     4/16/2016
// Author:      Anirudh Ghantasala
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: input buffer where data from the master will be stored

module input_buffer(
    input wire clk,
    input wire n_rst,
    input wire [63:0] data_i,
    input wire [3:0] input_data_select,
    input wire transfer_complete,
    output reg data_ready,
    output wire [511:0] i_data,
    output wire [63:0] m_data
);

    reg [575:0] in_buf, next_in_buf;
    reg next_data_ready;
    
    always_ff @ (posedge clk, negedge n_rst)
    begin
        if(~n_rst)
        begin
            in_buf <= 0;
            data_ready <= 0;
        end
        else
        begin
            in_buf <= next_in_buf;
            data_ready <= transfer_complete; // changes from master
        end
    end
    
    // next in buf is continually changing
    always_comb begin: store_values
       next_in_buf = in_buf;
   // if(transfer_complete)
        case(input_data_select)
        4'b0000: next_in_buf[63:0]    = data_i;
        4'b0001: next_in_buf[127:64]  = data_i;
        4'b0010: next_in_buf[191:128] = data_i;
        4'b0011: next_in_buf[255:192] = data_i;
        4'b0100: next_in_buf[319:256] = data_i;
        4'b0101: next_in_buf[383:320] = data_i;
        4'b0110: next_in_buf[447:384] = data_i;
        4'b0111: next_in_buf[511:448] = data_i;
        4'b1000: next_in_buf[575:512] = data_i;
        default: 
            begin
                next_in_buf = in_buf;  // when in_data_select > 9
            end
        endcase
    end
    
    assign m_data = in_buf[63:0];
    assign i_data = in_buf[575:64];
    
endmodule

    