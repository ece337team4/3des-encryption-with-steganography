// $Id: test bench for master
// File name:   tb_master.sv
// Created:     4/12/2016
// Author:      Anirudh Ghantasala
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: test bench for master for the final project, inputs an address, sees if master reads and writes to address

`timescale 1ns / 100ps

module tb_master(
);

	reg tb_clk;
	reg tb_n_rst;
	reg tb_hresp;
	reg tb_hready;
	reg tb_e_re;
	reg tb_e_we;
	reg [19]tb_send_addr;
	reg tb_transfer;
	reg [64]tb_data_out;
	reg [64]tb_hrdata;
	reg [64]tb_hwdata;
	reg [3]tb_hsize;
	reg tb_hwrite;
	reg [3]tb_hburst;
	reg [4]tb_hprot;
	reg [19]tb_haddr;
	reg tb_hmastblock;
	reg [2]tb_htrans;
	reg tb_controller_hresp;		// wait a clock cycle if hresp is 1 (slave needs more time)
	reg [64] tb_data_in;


	always begin: clock
		#2 tb_clk = 0;
		#2 tb_clk = 1;
	end

	//registers for RAM data
	reg [999:0] RAM;
	
	master FUT (.HRESP(tb_HRESP), .HREADY(tb_HREADY), .e_re(tb_e_re), .e_we(tb_e_we), .send_addr(tb_send_addr), .transfer(tb_transfer), .data_out(tb_data_out, .HRDATA(tb_HRDATA), .HWDATA(tb_HWDATA), 
				.HSIZE(tb_HSIZE), .HWRITE(tb_HWRITE), .HBURST(tb_HBURST), .HPROT(tb_HPROT), .HADDR(tb_HADDR), .HMASTBLOCK(tb_HMASTBLOCK), .HTRANS(tb_HTRANS), .controller_hresp(tb_controller_hresp), .data_in(tb_data_in));



	
initial begin
   tb_n_rst = 1;
	tb_hburst = 0; //only single transfers
	tb_hready = 1;
	@(negedge tb_clk);					//initial reset of all variables
	tb_n_rst = 0;
	@(negedge tb_clk);
	tb_n_rst = 1;



// WRITE DATA TO RAM-----------------------------------------------------
	@(negedge tb_clk);
	tb_haddr = 19'd1;					// first address phase
	tb_hrdata = 1;
   tb_htrans = 2'd2;// nonsequential
	tb_hsize = 3'd3; // 64 bits
	tb_hwdata = 0;
	tb_hready = 1;

	@(negedge tb_clk);
	tb_haddr = 19'd0;					// first data phase
	tb_hrdata = 64'd11;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

// READ DATA FROM RAM-----------------------------------------------------
	@(negedge tb_clk);
	tb_haddr = 19'd1;					// first address phase
	tb_hrdata = 1;
   tb_htrans = 2'd2;// nonsequential
	tb_hsize = 3'd3; // 64 bits
	tb_hwdata = 0;
	tb_hready = 1;

	@(negedge tb_clk);
	#1;							// data comes little later than next clk cycle
	tb_haddr = 19'd0;					// first data phase
	tb_hrdata = 64'd11;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr = 19'd2;					// second address phase
	tb_hwrite = 1;
   tb_htrans = 2'd2;// nonsequential
	tb_hsize = 3'd3; // 64 bits	
	tb_hready = 1;
	tb_hrdata = '0;
    
	@(negedge tb_clk);
	tb_haddr = 19'd0;					// second data phase
	tb_hrdata = 64'd22;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr = 19'd3;					// third address phase
	tb_hwrite = 1;
   tb_htrans = 2'd2;// nonsequential
	tb_hsize = 3'd3; // 64 bits	
	tb_hready = 1;
	tb_hrdata = '0;
	
	@(negedge tb_clk);
	tb_haddr = 19'd0;					// third data phase
	tb_hrdata = 64'd33;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr = 19'd4;					// fourth address phase
	tb_hwrite = 1;
   tb_htrans = 2'd2;// nonsequential
	tb_hsize = 3'd3; // 64 bits	
	tb_hready = 1;
	tb_hrdata = '0;

	@(negedge tb_clk);
	tb_haddr = 19'd0;					// fourth data phase
	tb_hrdata = 64'd44;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;
	
	@(negedge tb_clk);
	tb_haddr = 19'd5;					// fifth address phase
	tb_hrdata = 64'd0;					
	tb_hwrite = 0;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr = 19'd0;					// fifth data phase
	tb_hrdata = 64'd338;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr = 19'd6;					// sixth address phase
	tb_hrdata = 64'd0;					
	tb_hwrite = 0;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr = 19'd0;					// sixth data phase
	tb_hrdata = 64'd226;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;


   end
   
endmodule // tb_slave
	
	





















