// $Id: $
// File name:   straigh_dbox.sv
// Created:     4/19/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: 32 straight permutation for DES function

module straight_dbox
  (
   input wire [31:0] in,
   output reg [31:0] out

   );

   always @ (in)
     begin
	out[31] = in[16];//16
	out[30] = in[25];//07
	out[29] = in[12];//20
	out[28] = in[11];//21
	out[27] = in[03];//29
	out[26] = in[20];//12
	out[25] = in[04];//28
	out[24] = in[15];//17
	out[23] = in[31];//01
	out[22] = in[17];//15
	out[21] = in[09];//23
	out[20] = in[06];//26
	out[19] = in[27];//05
	out[18] = in[14];//18
	out[17] = in[01];//31
	out[16] = in[22];//10
	out[15] = in[30];//02
	out[14] = in[24];//08
	out[13] = in[08];//24
	out[12] = in[18];//14
	out[11] = in[00];//32
	out[10] = in[05];//27
	out[09] = in[29];//03
	out[08] = in[23];//09
	out[07] = in[13];//19
	out[06] = in[19];//13
	out[05] = in[02];//30
	out[04] = in[26];//06
	out[03] = in[10];//22
	out[02] = in[21];//11
	out[01] = in[28];//04
	out[00] = in[07];//25

     end // always @ (in)
endmodule // straight_dbox
