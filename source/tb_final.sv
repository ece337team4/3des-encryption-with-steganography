// $Id: test bench final
// File name:   final.sv
// Created:     4/16/2016
// Author:      Anirudh Ghantasala
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: test bench for the entire program


module tb_final(

	input reg [63:0]data_out,
	input reg HWDATA,
	input reg [2:0] HSIZE,
	input reg HWRITE,
	input reg [2:0] HBURST,
	input reg [3:0] HPROT,
	input reg [18:0] HADDR,
	input reg HMASTBLOCK,	
	input wire [1:0] HTRANS,
	input wire controller_hresp,
	
	
	output reg [63:0] data_in,
	output reg slave_information // encryption, address, key, start

);



controller DUT (.clk(tb_clk), .n_rst(tb_n_rst), .start_addr(tb_start_addr), .encryption(tb_encryption), .data_ready(tb_data_ready), .data_encrypted(tb_data_encrypted), .data_steg(tb_data_steg), .data_done(tb_data_done), .count(tb_count), .out_data_select(tb_out_data_select), .e_re(tb_e_re), .e_we(tb_e_we), .transfer(tb_transfer), .new_i_data(tb_new_i_data), .in_data_select(tb_in_data_select), .encrypt(tb_encrypt), .new_m_data(tb_new_m_data), .addr_send(tb_addr_send), .start(tb_start));

master FUT (.HRESP(tb_HRESP), .HREADY(tb_HREADY), .e_re(tb_e_re), .e_we(tb_e_we), .send_addr(tb_send_addr), .transfer(tb_transfer), .data_out(tb_data_out, .HRDATA(tb_HRDATA), .HWDATA(tb_HWDATA),	.HSIZE(tb_HSIZE), .HWRITE(tb_HWRITE), .HBURST(tb_HBURST), .HPROT(tb_HPROT), .HADDR(tb_HADDR), .HMASTBLOCK(tb_HMASTBLOCK), .HTRANS(tb_HTRANS), .controller_hresp							(tb_controller_hresp), .data_in(tb_data_in));


