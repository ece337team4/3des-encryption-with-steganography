// $Id: $
// File name:   tb_des_round_toplevel.sv
// Created:     4/19/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Test bench for encryption round

module tb_des_round_toplevel
  ();
   timeunit 1ns;
   reg tb_clk;
   localparam CLK_PERIOD = 10ns;
   wire [47:0] tb_round_key;
   wire [63:0] tb_plaintext;
   wire [63:0] cipher_out;
   reg [47:0]  tb_test_key;
   reg [63:0]  tb_test_plaintext;

   assign tb_round_key = tb_test_key;
   assign tb_plaintext = tb_test_plaintext;

   des_round_toplevel DUT(
			  .data_msg(tb_plaintext),
			  .round_key(tb_round_key),
			  .cipher_out(cipher_out)
			  );

   always begin
      tb_clk = 0;
      #(CLK_PERIOD);
      tb_clk = 1;
      #(CLK_PERIOD);
        
   end
   
initial begin
      tb_test_key[47:0] = 48'h194cd072de8c;
      tb_test_plaintext[63:0] = 64'h14a7d67818ca18ad;
      @(posedge tb_clk);

      tb_test_key[47:0] = 48'h4568581abcce;
      tb_test_plaintext[63:0] = 64'h18ca18ad5a78e394;
      @(posedge tb_clk);

      tb_test_key[47:0] = 48'h06eda4acf5b5;
      tb_test_plaintext[63:0] = 64'h5a78e3944a1210f6;
      @(posedge tb_clk);

      @(posedge tb_clk);
      @(posedge tb_clk);
      
      
      #1;
      
   end
   
endmodule // tb_des_round_toplevel
