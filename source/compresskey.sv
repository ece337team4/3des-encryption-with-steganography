// $Id: $
// File name:   compresskey.sv
// Created:     4/17/2016
// Author:      Akshay Sanjeev Daftardar
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: rotates and compresses the key
module compresskey
(
	input wire clk,
	input wire n_rst,
	input wire [55:0] midkey1,
	input wire encrypt,
	input wire sendready,
	input wire [4:0] round,
	output reg [47:0] laststp,
	output reg [55:0] currmidkey
);
reg [27:0] left;
reg [27:0] right;
reg [55:0] retmidkey;
always_ff @(posedge clk, negedge n_rst) 
begin
	if (!n_rst)
	begin
		currmidkey <= 56'b0;
	end
	else
	begin
		currmidkey <= retmidkey;
	end
end
reg clear;
reg rollover_flag;
always_comb
begin
	if (encrypt == 1)
	begin
		if (sendready == 1)
		begin
			if (round == 5'd1 || round == 5'd2 || round == 5'd9 || round == 5'd16)
			begin	
				left = {midkey1[54:28],midkey1[55]};
				right = {midkey1[26:0],midkey1[27]};
				retmidkey = {left, right};
	
			end		
			else if (round == 5'd0)
			begin
				retmidkey = midkey1;
			end
			else
			begin
				left = {midkey1[53:28],midkey1[55:54]};
				right = {midkey1[25:0],midkey1[27:26]};
				retmidkey = {left, right};
			end
		end
		else
		begin
			retmidkey = midkey1;
		end
	end
	else
	begin
		if (sendready == 1)
		begin
			if (round == 5'd1 || round == 5'd0)
			begin
				retmidkey = midkey1;
			end
			else if (round == 5'd2 || round == 5'd9 || round == 5'd16)
			begin
				left = {midkey1[28],midkey1[55:29]};
				right = {midkey1[0],midkey1[27:1]};
				retmidkey = {left, right};
			end
			else	
			begin
				left = {midkey1[29:28],midkey1[55:30]};
				right = {midkey1[1:0],midkey1[27:2]};
				retmidkey = {left, right}; 
			end
		end
		else
		begin
			retmidkey = midkey1;
		end
	end
end
	always_comb
	begin
		laststp[47] = retmidkey[42];//14
		laststp[46] = retmidkey[39];//17
		laststp[45] = retmidkey[45];//11
		laststp[44] = retmidkey[32];//24
		laststp[43] = retmidkey[55];//01 
		laststp[42] = retmidkey[51];//05
		laststp[41] = retmidkey[53];//03
		laststp[40] = retmidkey[28];//28

		laststp[39] = retmidkey[41];//15
		laststp[38] = retmidkey[50];//06
		laststp[37] = retmidkey[35];//21 
		laststp[36] = retmidkey[46];//10 
		laststp[35] = retmidkey[33];//23
		laststp[34] = retmidkey[37];//19
		laststp[33] = retmidkey[44];//12 
		laststp[32] = retmidkey[52];//04 

		laststp[31] = retmidkey[30];//26 
		laststp[30] = retmidkey[48];//08 
		laststp[29] = retmidkey[40];//16 
		laststp[28] = retmidkey[49];//07 
		laststp[27] = retmidkey[29];//27 
		laststp[26] = retmidkey[36];//20 
		laststp[25] = retmidkey[43];//13  
		laststp[24] = retmidkey[54];//02
	
		laststp[23] = retmidkey[15];//41 
		laststp[22] = retmidkey[4];//52 
		laststp[21] = retmidkey[25];//31   
		laststp[20] = retmidkey[19];//37  
		laststp[19] = retmidkey[9];//47 
		laststp[18] = retmidkey[1];//55 
		laststp[17] = retmidkey[26];//30   
		laststp[16] = retmidkey[16];//40  

		laststp[15] = retmidkey[5];//51 
		laststp[14] = retmidkey[11];//45 
		laststp[13] = retmidkey[23];//33     
		laststp[12] = retmidkey[8];//48    
		laststp[11] = retmidkey[12];//44 
		laststp[10] = retmidkey[7];//49   
		laststp[9] = retmidkey[17];//39   
		laststp[8] = retmidkey[0];//56   

		laststp[7] = retmidkey[22];//34 
		laststp[6] = retmidkey[3];//53  
		laststp[5] = retmidkey[10];//46     
		laststp[4] = retmidkey[14];//42   
		laststp[3] = retmidkey[6];//50 
		laststp[2] = retmidkey[20];//36   
		laststp[1] = retmidkey[27];//29    
		laststp[0] = retmidkey[24];//32
	end  
endmodule
		
		
