// $Id: $
// File name:   tb_encryption_toplevel.sv
// Created:     4/25/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Test bench for encryption toplevel
`timescale 1ns/100ps

module tb_encryption_toplevel();
   timeunit 1ns;
   localparam CLK_PERIOD = 10ns;
   reg tb_clk;
   reg tb_n_rst;
   reg tb_start;
   reg [63:0] tb_plain_text;
   reg [63:0] tb_full_key;
   reg tb_encrypt;
   reg [63:0] tb_cipher_text;
   reg 	      stage_done;
   
   
   encryption_toplevel DUT(
			   .clk(tb_clk),
			   .n_rst(tb_n_rst),
			   .plain_text(tb_plain_text),
			   .start(tb_start),
			   .full_key(tb_full_key),
			   .encrypt(tb_encrypt),
			   .cipher_text(tb_cipher_text),
			   .stage_done(stage_done)
	       );
   always begin: clock
      tb_clk = 0;
      #(CLK_PERIOD/2);
      tb_clk = 1;
      #(CLK_PERIOD/2);
   end


   initial
     begin
	
	@(negedge tb_clk);
	tb_n_rst = 0;
	@(negedge tb_clk);
	tb_n_rst = 1;
	
	tb_encrypt= 0;
	tb_n_rst = 1;
	tb_start = 0;
	
	
	
	tb_plain_text = 64'h123456abcd132536;
	tb_full_key = 64'h3b3898371520f75e;
	//tb_plain_text = 64'h123456abcd132536;
	//tb_full_key = 64'haabb09182736ccdd;
	
			
	@(negedge tb_clk);
	tb_start = 1;
	@(negedge tb_clk);
	tb_start = 0;
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	@(negedge tb_clk);
	tb_n_rst = 0;
	
	
     end // initial begin
   
	
	  

endmodule // tb_encryption_toplevel
