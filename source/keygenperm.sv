// $Id: $
// File name:   keygenperm.sv
// Created:     4/16/2016
// Author:      Akshay Sanjeev Daftardar
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: key generation permutations
module keygenperm
(
	input wire [63:0] key,
	input wire [4:0] round,
	input wire [55:0] currmid,
	output reg [55:0] midkey
);
always_comb
begin
	if (round == 5'd1)
	begin
		midkey[55] = key[7];//57
		midkey[54] = key[15];//49 
		midkey[53] = key[23];//41
		midkey[52] = key[31];//33
		midkey[51] = key[39];//25
		midkey[50] = key[47];//17
		midkey[49] = key[55];//09
		midkey[48] = key[63];//01
		midkey[47] = key[6];//58
		midkey[46] = key[14];//50
		midkey[45] = key[22];//42
		midkey[44] = key[30];//34
		midkey[43] = key[38];//26
		midkey[42] = key[46];//18 
		midkey[41] = key[54];//10
		midkey[40] = key[62];//02

		midkey[39] = key[5];//59
		midkey[38] = key[13];//51
		midkey[37] = key[21];//43
		midkey[36] = key[29];//35
		midkey[35] = key[37];//27
		midkey[34] = key[45];//19
		midkey[33] = key[53];//11
		midkey[32] = key[61];//03
		midkey[31] = key[4];//60
		midkey[30] = key[12];//52 
		midkey[29] = key[20];//44
		midkey[28] = key[28];//36 
		midkey[27] = key[1];//63
		midkey[26] = key[9];//55
		midkey[25] = key[17];//47
		midkey[24] = key[25];//39
		midkey[23] = key[33];//31
		
		midkey[22] = key[41];//23
		midkey[21] = key[49];//15
		midkey[20] = key[57];//07
		midkey[19] = key[2];//62
		midkey[18] = key[10];//54
		midkey[17] = key[18];//46 
		midkey[16] = key[26];//38		
		midkey[15] = key[34];//30
		midkey[14] = key[42];//22
		midkey[13] = key[50];//14
		midkey[12] = key[58];//06
		midkey[11] = key[3];//61
		midkey[10] = key[11];//53
		midkey[9] = key[19];//45
		midkey[8] = key[27];//37
		midkey[7] = key[35];//29
		midkey[6] = key[43];//21
		midkey[5] = key[51];//13
		midkey[4] = key[59];//05
		midkey[3] = key[36];//28
		midkey[2] = key[44];//20
		midkey[1] = key[52];//12
		midkey[0] = key[60];//04
	end
	else
	begin
		midkey = currmid;
	end
end 		
endmodule
