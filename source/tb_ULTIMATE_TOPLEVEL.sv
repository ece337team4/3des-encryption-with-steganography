// ENCRYPTION TESTBENCH
// $Id: $
// File name:   tb_ultimate_toplevel.sv
// Created:     4/16/2016
// Author:      Ayush Malhotra the boss
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: tb for controller fsm, the brains of the entire program

`timescale 1ns / 100ps

module tb_ULTIMATE_TOPLEVEL();

   reg 		  tb_clk=0;
   reg 		  tb_n_rst;
   reg  		  tb_M_HRESP;
   reg 		  tb_M_HREADY;
   reg [18:0] tb_M_HADDR;
   reg [63:0] tb_M_HRDATA;
   reg [2:0]  tb_M_HBURST;
   reg [2:0]  tb_M_HSIZE;
   reg [3:0]  tb_M_HPROT;
   reg 	     tb_M_HMASTLOCK; 	    
   reg [1:0]  tb_M_HTRANS;
   reg 	     tb_M_HWRITE;
   reg [63:0] tb_M_HWDATA;
   reg 	     tb_M_hsel;
   reg 	     tb_S_HMASTLOCK;
   reg [2:0]  tb_S_HBURST;
   reg [3:0]  tb_S_HPROT;
   reg [2:0]  tb_S_HSIZE;
   reg 	     tb_S_HREADY;
   reg 	     tb_S_HREADYOUT;
   reg 	     tb_S_HRESP;
   reg [63:0] tb_S_HWDATA;
   reg [1:0]  tb_S_HTRANS;
   reg [18:0] tb_S_HADDR;
   reg 	      tb_S_HWRITE;
   reg 		  tb_S_hsel1;
	reg [575:0] out_buf;

   always begin: clock
      #5 tb_clk = 0;
      #5 tb_clk = 1;
   end

   
   ULTIMATE_TOPLEVEL DUT (.clk(tb_clk), .n_rst(tb_n_rst), .M_HRESP(tb_M_HRESP), .M_HREADY(tb_M_HREADY), .M_HRDATA(tb_M_HRDATA), .M_HADDR(tb_M_HADDR), .M_HBURST(tb_M_HBURST), .M_HSIZE(tb_M_HSIZE), .M_HPROT(tb_M_HPROT), .M_HMASTLOCK	(tb_M_HMASTLOCK), .M_HTRANS(tb_M_HTRANS), .M_HWRITE(tb_M_HWRITE), .M_HWDATA(tb_M_HWDATA), .S_hsel1(tb_S_hsel1), .S_HWRITE(tb_S_HWRITE), .S_HBURST(tb_S_HBURST), .S_HPROT(tb_S_HPROT), .S_HSIZE(tb_S_HSIZE), .S_HMASTLOCK(tb_S_HMASTLOCK), .S_HREADY(tb_S_HREADY), .S_HREADYOUT(tb_S_HREADYOUT), .S_HRESP(tb_S_HRESP), .S_HWDATA(tb_S_HWDATA),.S_HADDR(tb_S_HADDR), .S_HTRANS(tb_S_HTRANS));
   

 /*  reg [1000 : 0] RAM;
   // update the data on M_HRDATA following every address phase
	always_ff @(posedge tb_clk, negedge tb_n_rst)		// DONT WE WANT THE DATA TO COME ONE CYCLE AFTER THE ADDRESS IS PROVIDED? SO THE ALWAYS FF WOULD WORK?
   begin
		if(~tb_n_rst)
			tb_M_HRDATA <= 0;
		else
			tb_M_HRDATA <= n_tb_M_HRDATA;
	end

	always_comb begin: update m_hrdata
		if(transfer && ~tb_M_HWRITE) // master is requesting a read
   		tb_M_HRDATA = RAM[tb_M_HADDR+64:tb_M_HADDR]; 
		else
			tb_M_HRDATA = 0;
	end*/


///////////INITIAL BLOCK FOR DECRYPTION///////////////////////
/*
initial
   begin
   tb_n_rst = 1;
	tb_S_HBURST = 0; //only single transfers
	tb_S_HREADY = 1;
	@(negedge tb_clk);					//initial reset of all variables
	tb_n_rst = 0;
	@(negedge tb_clk);
	tb_n_rst = 1;

	// SLAVE INTIALIZATIONS------------------------------------------------------------------------------------------------
	@(negedge tb_clk);
	tb_S_hsel1 = 1;
	tb_S_HADDR = 19'd2;					// first address phase
	tb_S_HWRITE = 1;
   tb_S_HTRANS = 2'd2;// nonsequential
	tb_S_HSIZE = 3'd3; // 64 bits
	tb_S_HWDATA = 0;
	tb_S_HREADY = 1;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// first data phase
	tb_S_HWDATA = 64'd0;//encrypt;			
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd3;					// second address phase
	tb_S_HWRITE = 1;
   tb_S_HTRANS = 2'd2;
	tb_S_HSIZE = 3'd3; 	
	tb_S_HREADY = 1;
	tb_S_HWDATA = '0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// second data phase
	tb_S_HWDATA = 64'd1;//start_addr					
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd4;					// third address phase
	tb_S_HWRITE = 1;
   tb_S_HTRANS = 2'd2;
	tb_S_HSIZE = 3'd3; 	
	tb_S_HREADY = 1;
	tb_S_HWDATA = '0;
	
	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// third data phase
	tb_S_HWDATA = 64'h3b3898371520f75e;//key1					
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd5;					// fourth address phase
	tb_S_HWRITE = 1;
   tb_S_HTRANS = 2'd2;
	tb_S_HSIZE = 3'd3; 
	tb_S_HREADY = 1;
	tb_S_HWDATA = '0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// fourth data phase
	tb_S_HWDATA = 64'h922fb510c71f436e;	//key2				
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;
	
	@(negedge tb_clk);
	tb_S_HADDR = 19'd6;					// fifth address phase
	tb_S_HWDATA = 64'd0;					
	tb_S_HWRITE = 0;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// fifth data phase
	tb_S_HWDATA = 64'h3b3898371520f75e; //key3					
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd1;					// sixth address phase
	tb_S_HWDATA = 64'd0;					
	tb_S_HWRITE = 0;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// sixth data phase
	tb_S_HWDATA = 64'd1;	//start				
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;


   // CONTROLLER PHASE ------------------------------------------------------------------------------------------------
	@(negedge tb_clk);
	//master will communicate with testbench to gain data at the address supplied to the slave above
	
	// read operations carry themselves out
       // if (tb_M_HWRITE == 0 && tb_M_HADDR == 'd1)
//	{
	 
	// #(2.5)
	// tb_M_HRDATA = 64'habcde12345abcde6;
//	 }
	@(negedge tb_clk);
       // if (tb_M_HWRITE == 0 && tb_M_HADDR == 'd1)
//	{
	 
	// #(2.5)
	 @(negedge tb_clk);
	 tb_M_HRDATA = 64'habcde12345abcde6;
//	 }	// read message
	@(negedge tb_clk);	

      tb_M_HRDATA = 64'd1;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd2;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd3;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd4;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd5;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd6;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd7;
		
		@(negedge tb_clk);
      tb_M_HRDATA = 64'd0;


		// 26 wait states till next image data is required
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);	
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);	
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);	
		@(negedge tb_clk);




	 // SECOND BATCH
	 @(negedge tb_clk);
	 tb_M_HRDATA = 64'h12345abcde123456;
//	 }	// read message
	@(negedge tb_clk);	

      tb_M_HRDATA = 64'd22;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd33;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd44;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd55;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd66;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd77;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd88;
		
		@(negedge tb_clk);
      tb_M_HRDATA = 64'd0;
      





      
	   end
*/


//////////INITIAL BLOCK FOR ENCRYPTION TB////////////

initial
   begin
   tb_n_rst = 1;
	tb_S_HBURST = 0; //only single transfers
	tb_S_HREADY = 1;
	@(negedge tb_clk);					//initial reset of all variables
	tb_n_rst = 0;
	@(negedge tb_clk);
	tb_n_rst = 1;

	// SLAVE INTIALIZATIONS------------------------------------------------------------------------------------------------
	@(negedge tb_clk);
	tb_S_HADDR = 19'd2;					// first address phase
	tb_S_HWRITE = 1;
   tb_S_HTRANS = 2'd2;// nonsequential
	tb_S_HSIZE = 3'd3; // 64 bits
	tb_S_HWDATA = 0;
	tb_S_HREADY = 1;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// first data phase
	tb_S_HWDATA = 64'd1;//encrypt;			
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd3;					// second address phase
	tb_S_HWRITE = 1;
   tb_S_HTRANS = 2'd2;
	tb_S_HSIZE = 3'd3; 	
	tb_S_HREADY = 1;
	tb_S_HWDATA = '0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// second data phase
	tb_S_HWDATA = 64'd1;//start_addr					
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd4;					// third address phase
	tb_S_HWRITE = 1;
   tb_S_HTRANS = 2'd2;
	tb_S_HSIZE = 3'd3; 	
	tb_S_HREADY = 1;
	tb_S_HWDATA = '0;
	
	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// third data phase
	tb_S_HWDATA = 64'h3b3898371520f75e;//key1					
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd5;					// fourth address phase
	tb_S_HWRITE = 1;
   tb_S_HTRANS = 2'd2;
	tb_S_HSIZE = 3'd3; 
	tb_S_HREADY = 1;
	tb_S_HWDATA = '0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// fourth data phase
	tb_S_HWDATA = 64'h922fb510c71f436e;	//key2				
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;
	
	@(negedge tb_clk);
	tb_S_HADDR = 19'd6;					// fifth address phase
	tb_S_HWDATA = 64'd0;					
	tb_S_HWRITE = 0;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// fifth data phase
	tb_S_HWDATA = 64'h3b3898371520f75e; //key3					
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd1;					// sixth address phase
	tb_S_HWDATA = 64'd0;					
	tb_S_HWRITE = 0;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;

	@(negedge tb_clk);
	tb_S_HADDR = 19'd0;					// sixth data phase
	tb_S_HWDATA = 64'd1;	//start				
	tb_S_HWRITE = 1;
	tb_S_HREADY = 0;
	tb_S_HTRANS = 0;
	tb_S_HSIZE = 3'd0;


   // CONTROLLER PHASE ------------------------------------------------------------------------------------------------
	@(negedge tb_clk);
	//master will communicate with testbench to gain data at the address supplied to the slave above
	
	// read operations carry themselves out
       // if (tb_M_HWRITE == 0 && tb_M_HADDR == 'd1)
//	{
	 
	// #(2.5)
	// tb_M_HRDATA = 64'habcde12345abcde6;
//	 }
	@(negedge tb_clk);
       // if (tb_M_HWRITE == 0 && tb_M_HADDR == 'd1)
//	{
	 
	// #(2.5)
	 tb_M_HRDATA = 64'habcde12345abcde6;
//	 }	// read message
	@(negedge tb_clk);	
	@(negedge tb_clk);

	@(negedge tb_clk);
	@(negedge tb_clk);

      tb_M_HRDATA = 64'd1;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd2;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd3;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd4;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd5;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd6;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd7;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd8;
      
		 @(negedge tb_clk);
      tb_M_HRDATA = 64'd0;
      
	   
   	//wait states till next image data is required
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);	
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);	
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);
		@(negedge tb_clk);	
		@(negedge tb_clk);




	 // SECOND BATCH
	 @(negedge tb_clk);
	 tb_M_HRDATA = 64'h12345abcde123456;
//	 }	// read message
	@(negedge tb_clk);	
	@(negedge tb_clk);
		@(negedge tb_clk);	
		@(negedge tb_clk);


      tb_M_HRDATA = 64'd22;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd33;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd44;
      
	@(negedge tb_clk);
      tb_M_HRDATA = 64'd55;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd66;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd77;

      @(negedge tb_clk);
      tb_M_HRDATA = 64'd88;
		
		@(negedge tb_clk);
      tb_M_HRDATA = 64'd0;
      























end


endmodule


