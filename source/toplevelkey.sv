// $Id: $
// File name:   toplevelkey.sv
// Created:     4/17/2016
// Author:      Akshay Sanjeev Daftardar
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: top level file for key generation
module toplevelkey
(
	input wire clk,
	input wire n_rst,
	input wire [63:0] key,
	input wire encrypt,
	input wire sendready,
	output reg [47:0] roundkey
);
reg [55:0] finmidkey;
reg [55:0] currmidkey;
reg clear;
reg rolloverflag;
reg [4:0] round;
flex_counter #(5) FLEX(.clk(clk),.n_rst(n_rst),.clear(clear),.count_enable(sendready),.rollover_val(5'd17),.count_out(round),.rollover_flag(rolloverflag));
keygenperm perm(.key(key), .round(round), .currmid(currmidkey), .midkey(finmidkey));
compresskey ckey(.clk(clk), .n_rst(n_rst), .midkey1(finmidkey),.encrypt(encrypt),.sendready(sendready), .round(round), .laststp(roundkey), .currmidkey(currmidkey)); 
endmodule 
