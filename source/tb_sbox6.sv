// $Id: $
// File name:   tb_sbox6.sv
// Created:     4/16/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: SBOX6 Test Bench

`timescale 10ns/100ps


module tb_sbox6
  ();   
   wire [5:0]tb_in;
   wire [3:0]tb_out;
   reg [3:0] tb_test_top;
   reg [1:0] tb_test_left;
   
   
   assign tb_in = {tb_test_left[1],tb_test_top[3:0], tb_test_left[0]};
   
   
   sbox6 DUT(
	    .in(tb_in),
	    .out(tb_out)
	    );
   
    initial begin
      
      tb_test_left[1:0] = 2'b00;
      
      repeat (4) begin
	 tb_test_top = 4'b0000;
	 
	   repeat (16) begin
	      #1;
		tb_test_top = tb_test_top + 1'b1;
	      
	     end
	   tb_test_left = tb_test_left + 1'b1;
	end
       
      
    end // initial begin
   
endmodule // tb_sbox6
