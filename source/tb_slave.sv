// $Id: $
// File name:   tb_slave.sv
// Created:     4/12/2016
// Author:      Anirudh Ghantasala
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: test bench for slave that communicates with cpu


module tb_slave
  ();
   timeunit 1ns;
   localparam CLK_PERIOD = 10ns;
   reg tb_hsel1;
   reg tb_hwrite;
   reg [2:0] tb_hburst;
   reg [3:0] tb_hprot;
	reg [2:0] tb_hsize;
	reg [1:0] tb_htrans;
	reg tb_hmastlock2;
	reg tb_hready;
	reg tb_clk;
	reg tb_n_rst;
	reg [18:0] tb_haddr2;		// tells the slave where to store the data coming in in the next cycle
	reg tb_hreadyout;
	reg tb_hresp;
	reg [31:0]tb_hrdata;
	reg [63:0]tb_start;
	reg [63:0]tb_enorde;
	reg [63:0] tb_start_addr;
	reg [63:0] tb_enc_key;
	//standard
	reg [63:0]tb_hwdata;
  
   slave FUT(.clk(tb_clk), .n_rst(tb_n_rst), .hsel1(tb_hsel1), .hwrite(tb_hwrite), .hburst(tb_hburst), .hprot(tb_hprot), .hsize(tb_hsize), .hmastlock2(tb_hmastlock2), .htrans(tb_htrans), .hready(tb_hready), .hreadyout(tb_hreadyout), .hresp(tb_hresp), .hwdata(tb_hwdata), .haddr2(tb_haddr2), .enorde(tb_enorde)
,.start(tb_start), .enc_key(tb_enc_key), .start_addr(tb_start_addr));

   always begin
      tb_clk = 0;
      #(CLK_PERIOD);
      tb_clk = 1;
      #(CLK_PERIOD);
        
   end
   
initial begin
   tb_n_rst = 1;
	tb_hburst = 0; //only single transfers
	tb_hready = 1;
	@(negedge tb_clk);					//initial reset of all variables
	tb_n_rst = 0;
	@(negedge tb_clk);
	tb_n_rst = 1;

	@(negedge tb_clk);
	tb_haddr2 = 19'd1;					// first address phase
	tb_hwrite = 1;
   tb_htrans = 2'd2;// nonsequential
	tb_hsize = 3'd3; // 64 bits
	tb_hwdata = 0;
	tb_hready = 1;

	@(negedge tb_clk);
	tb_haddr2 = 19'd0;					// first data phase
	tb_hwdata = 64'd11;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr2 = 19'd2;					// second address phase
	tb_hwrite = 1;
   tb_htrans = 2'd2;// nonsequential
	tb_hsize = 3'd3; // 64 bits	
	tb_hready = 1;
	tb_hwdata = '0;
      
	@(negedge tb_clk);
	tb_haddr2 = 19'd0;					// second data phase
	tb_hwdata = 64'd22;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr2 = 19'd3;					// third address phase
	tb_hwrite = 1;
   tb_htrans = 2'd2;// nonsequential
	tb_hsize = 3'd3; // 64 bits	
	tb_hready = 1;
	tb_hwdata = '0;
	
	@(negedge tb_clk);
	tb_haddr2 = 19'd0;					// third data phase
	tb_hwdata = 64'd33;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr2 = 19'd4;					// fourth address phase
	tb_hwrite = 1;
   tb_htrans = 2'd2;// nonsequential
	tb_hsize = 3'd3; // 64 bits	
	tb_hready = 1;
	tb_hwdata = '0;

	@(negedge tb_clk);
	tb_haddr2 = 19'd0;					// fourth data phase
	tb_hwdata = 64'd44;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;
	
	@(negedge tb_clk);
	tb_haddr2 = 19'd5;					// fifth address phase
	tb_hwdata = 64'd0;					
	tb_hwrite = 0;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr2 = 19'd0;					// fifth data phase
	tb_hwdata = 64'd338;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr2 = 19'd6;					// sixth address phase
	tb_hwdata = 64'd0;					
	tb_hwrite = 0;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;

	@(negedge tb_clk);
	tb_haddr2 = 19'd0;					// sixth data phase
	tb_hwdata = 64'd226;					
	tb_hwrite = 1;
	tb_hready = 0;
	tb_htrans = 0;
	tb_hsize = 3'd0;


   end
   
endmodule // tb_slave
