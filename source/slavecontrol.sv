// $Id: Controller
// File name:   controller.sv
// Created:     4/24/2016
// Author:      Ayush Malhotra
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: slave controller, coordinates actions between slave and main controller

module slavecontrol
(
input wire clk,
input wire n_rst,
input wire [63:0] hwdata_in,

output reg [18:0] start_addr,
output reg [191:0] enc_key,
output reg enorden,
output reg start
 );
   

   typedef enum logic [2:0] {IDLE, STARTUP, ENC, DEC, KEYUP1, KEYUP2, KEYUP3, SENDSIG} state_type;
   
state_type curstate, nextstate;
   reg [18:0] 	nextaddr;
   reg [18:0] 	curraddr;
   reg [191:0] 	currkey;
   reg [191:0] 	nextkey;
   reg 		currn;
   reg 		nextn;
   
   
always_ff @ (posedge clk, negedge n_rst)
begin
	if (~n_rst)
	begin
		curstate <= IDLE;
	   currkey <= 0;
	   currn <= 0;
	   curraddr <= 0;
	   	   
	end
	else
	begin
		curstate <= nextstate;
	   currkey <= nextkey;
	   curraddr <= nextaddr;
	   currn <= nextn;
	   
	   
	end
end

always_comb begin: CONTROLSLAVE


start = 0;
   nextkey = currkey;
   nextaddr = curraddr;
   nextn = currn;
   nextstate = curstate;
   
   
case(curstate)
IDLE:
begin
		if (hwdata_in == '1)
			nextstate = STARTUP;
		else
		begin
			nextstate = IDLE;
			nextaddr = 0;
			nextkey = currkey;
			nextn = 0;
			start = 0;
		end
end
STARTUP:
begin
		if (hwdata_in == 64'd1)
		begin
			nextstate = ENC;
			nextn = 1;
		end
		else if (hwdata_in == 64'd2)
		begin
			nextstate = DEC;
			nextn = 0;
		end
		else
		begin
			nextstate = STARTUP;
		end
		start = 0;
		nextkey = 0;
		nextaddr = 0;
end
ENC:
begin
	nextaddr = hwdata_in [18:0];
	nextkey = 0;
	nextn = 1;
	start = 0;
	nextstate = KEYUP1;
end

DEC:
begin
	nextaddr = hwdata_in [18:0];
	nextkey = 0;
	nextn = 0;
	start = 0;
	nextstate = KEYUP1;
end

KEYUP1:
  begin
	 nextkey [63:0] = hwdata_in;
	 nextn = currn;
	 start = 0;
	 nextstate = KEYUP2;
	 nextaddr = curraddr;
	 
  end

  KEYUP2:
	begin
	 nextkey [127:64] = hwdata_in;
	   nextn = currn;
	   start = 0;
	   nextstate = KEYUP3;
	   nextaddr = curraddr;
	   
	end

  KEYUP3:
	begin
	   nextkey [191:128] = hwdata_in;
	   nextn = currn;
	   start = 0;
	   nextstate = SENDSIG;
	   nextaddr = curraddr;       
	end

  SENDSIG:
	begin
	   nextkey = currkey;
	   nextn = currn;
	   start = 1;
	   nextstate = IDLE;
	   nextaddr = curraddr;
	end
 endcase
end


   assign start_addr = curraddr;
   assign enc_key = currkey;
   assign enorden = currn;

endmodule // slavecontrol

  
 
