// $Id: $
// File name:   tb_controller.sv
// Created:     4/16/2016
// Author:      Ayush Malhotra
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: tb for controller fsm, the brains of the entire program

`timescale 1ns / 100ps

module tb_controller(
);

	reg tb_clk=0;
	reg tb_n_rst;
	reg [18:0] tb_start_addr;
	reg tb_enorden;
	reg tb_data_ready;
	reg tb_new_data;
	reg tb_data_encrypted;
	reg tb_data_steg;
	reg tb_data_done;
	reg [6:0] tb_count;
	reg [3:0] tb_out_data_select;
	reg tb_e_re;
	reg tb_e_we;
	reg tb_transfer;
	reg tb_new_i_data;
	reg [3:0] tb_in_data_select;
	reg tb_encrypt;
	reg tb_new_m_data;
	reg [18:0] tb_addr_send;
	reg tb_start;

	reg [63:0]tb_hwdata_in;
	reg tb_hsel1;
	reg tb_HWRITE;
	reg tb_HBURST;
	reg tb_HPROT;
	reg tb_HSIZE;
	reg tb_HMASTLOCK;	
	reg tb_HREADY;
	reg tb_HREADYOUT;
	reg tb_HRESP;
	reg tb_HRDATA;
	reg [63:0] tb_HWDATA;
	reg [18:0]tb_HADDR;		// from master to ram

	reg [63:0]tb_data_out;	// from master to ram
	


	always begin: clock
		#5 tb_clk = 0;
		#5 tb_clk = 1;
	end

	//registers for RAM data
	/*reg [999:0] RAM;

	integer i;

	initial
	begin
		for(i = 0; i < 1000; i++)
		begin
			RAM[i] = {$random} % 2;			// RAM[i] = 1 or 0
		end

	end*/
	// at this point, ram is filled with random 1s and 0s


	controller DUT (.clk(tb_clk), .n_rst(tb_n_rst), .start_addr(tb_start_addr), .encryption(tb_enorden), .data_ready(tb_data_ready), .data_encrypted(tb_data_encrypted), .data_steg(tb_data_steg), .data_done(tb_data_done), .count(tb_count), .out_data_select(tb_out_data_select), .e_re(tb_e_re), .e_we(tb_e_we), .transfer(tb_transfer), .new_i_data(tb_new_i_data), .in_data_select(tb_in_data_select), .encrypt(tb_encrypt), .new_m_data(tb_new_m_data), .addr_send(tb_addr_send), .start(tb_start));

	slave FUT(.clk(tb_clk), .n_rst(tb_n_rst), .hsel1(tb_hsel1), .hwrite(tb_HWRITE), .hburst(tb_HBURST), .hprot(tb_HPROT), .hsize(tb_HSIZE), .hmastlock(tb_HMASTLOCK), .hready(tb_HREADY), .hreadyout(tb_HREADYOUT), .hresp(tb_HRESP), .hrdata(tb_HRDATA), .hwdata(tb_HWDATA), .hwdataout(tb_hwdata_in));
	

	slavecontrol MUT(.clk(tb_clk), .n_rst(tb_n_rst), .hwdata_in(tb_hwdata_in), .start_addr(tb_start_addr), .enc_key(tb_enc_key), .enorden(tb_enorden), .start(tb_start));

	master QUT (.HRESP(tb_HRESP), .HREADY(tb_HREADY), .e_re(tb_e_re), .e_we(tb_e_we), .send_addr(tb_addr_send), .transfer(tb_transfer), .data_out(tb_data_out), .HRDATA(tb_HRDATA), .HWDATA(tb_HWDATA), .HSIZE(tb_HSIZE), .HWRITE(tb_HWRITE), .HBURST(tb_HBURST), .HPROT(tb_HPROT), .HADDR(tb_HADDR), .HMASTBLOCK(tb_HMASTBLOCK), .HTRANS(tb_HTRANS), .controller_hresp(tb_controller_hresp), .data_in(tb_data_in));






	initial
	begin
		
		tb_n_rst = 1;
		@(negedge tb_clk)		
		tb_n_rst = 0;
		@(negedge tb_clk)		
		tb_n_rst = 1;		
		@(negedge tb_clk)
		@(negedge tb_clk)

		
		tb_HWDATA = '1;		// gets slave from idle to startup
		
		@(negedge tb_clk)
		@(negedge tb_clk)

		tb_HWDATA = 64'd1;		// gets slave from startup to encrypt(vs 2 for decrypt)

		@(negedge tb_clk)
		

		tb_HWDATA = 64'd1;		// start address, gets slave from encrypt to sendsig

		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		
		// shifts control to controller.sv should be in R1
		
		@(negedge tb_clk)
		//in R1wait
		
		@(negedge tb_clk)
		@(negedge tb_clk)
		
		tb_data_ready = 1;
		@(negedge tb_clk)
		tb_data_ready = 0;
		// ENCRYPT

		@(negedge tb_clk)
		// RIMG
		@(negedge tb_clk)
		// STORE
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
 		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		

		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
 		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		

		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)

		
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		

		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)


		// 48 negedges

		tb_data_encrypted = 1; // comes from encrypt block

		@(negedge tb_clk)
		// in steg
		@(negedge tb_clk)
		@(negedge tb_clk)
		tb_data_steg = 1;		
		@(negedge tb_clk)

		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)

		@(negedge tb_clk)



		tb_data_ready = 1;
		// back to R1


/*      decrypt testbench
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		
*/


		//if (tb_addr_send == 19'b0000000000000000001)								// R1 STAGE IN CONTROLLER
			//$info ("Correct address sent to master");
	//	if (tb_e_re == 1'b1)
	//		$info ("correctly enabled RE for reading in data");
		/*if (tb_transfer == 1'b1)
			$info ("correctly enabled transfer for reading in data");
		if (tb_in_data_select == 4'b0000)
			$info ("correct register chosen for input buffer to store data");*/
		/*
		$display ("Address being fetched by master for 1st MESSAGE PACKET : = %d", tb_addr_send);
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		tb_data_ready = 1;
		@(negedge tb_clk)

		if (tb_new_m_data == 1'b1)													// ENCRYPT STAGE IN CONTROLLER
			$info ("correctly sent new_m_data signal to encrypt block");
		if (tb_encrypt == 1'b1)
			$info ("correctly enabled encrypt signal to encrypt block");

		@(negedge tb_clk)
		
		if (tb_e_re == 1)															// RIMG
			$info ("correct enable RE for reading in 1ST IMAGE PACKET");
		if (tb_in_data_select == 4'd1)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master for 1st IMAGE PACKET : = %d", tb_addr_send);
		//SIMILARYLY SEND 7 MORE PACKETS OF IMAGE DATA

		//PACKET 2
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
		$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd2)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 3
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd3)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 4
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd4)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 5
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd5)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 6
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd6)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 7
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd7)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 8
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd8)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);	


		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)

		//WE ONLY WAITED FOR 3-4 MORE CYCLES, IN PRACTICAL IT WOULD TAKE ABOUT 30 or more CYCLES FOR ENCRYPTION
		//TO COMPLETE 

		@(negedge tb_clk)
		tb_data_encrypted = 1; //SIGNAL FROM ENCRYPTED BLOCK SAYING MESSAGE ENCRYPTED //ACTUAL SIGNAL FROM BLOCK COMES HERE

		@(negedge tb_clk)
		  
		if (tb_new_i_data == 1)
			$info ("correctly sent signal to steganography block to begin steg");

		//WAIT FOR STEGANOGRAPHY TO SEND SIGNAL "STEG DONE"
		@(negedge tb_clk)
		tb_data_steg = 1;
		@(negedge tb_clk)

		if (tb_e_we == 1)
			$info ("PACKET1: correctly received signal to write data (correct sent signal to master)");
		if (tb_out_data_select == 4'd1)
			$info ("Register 1 (of output buffer) correctly chosen");
		if (tb_transfer == 1)
			$info ("correctly received transfer signal (correct sent signal to master)");
		$display ("Address being send to master to write data to : = %d", tb_addr_send);	   

		@(negedge tb_clk)

		if (tb_e_we == 1)
			$info ("PACKET2: correctly received signal to write data (correct sent signal to master)");
		if (tb_out_data_select == 4'd2)
			$info ("Register 2 (of output buffer) correctly chosen");
		if (tb_transfer == 1)
			$info ("correctly received transfer signal (correct sent signal to master)");
		$display ("Address being send to master to write data to : = %d", tb_addr_send);

		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)

		$display ("skipping 5 cycles of writing image data, to last packet of img data");
		@(negedge tb_clk)
		if (tb_e_we == 1)
			$info ("PACKET8: correctly received signal to write data (correct sent signal to master)");
		if (tb_out_data_select == 4'd8)
			$info ("Register 8 (of output buffer) correctly chosen");
		if (tb_transfer == 1)
			$info ("correctly received transfer signal (correct sent signal to master)");
		$display ("Address being send to master to write data to : = %d", tb_addr_send);

		@(negedge tb_clk)

		//2nd PACKET OF MESSAGE (ENTIRE CYCLE STARTS AGAIN)
		if (tb_e_re == 1)
			$info ("correct enable RE for reading in 2nd part of MSG");
		if (tb_in_data_select == 4'd0)
			$info ("correct buffer register chosen for message data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		@(negedge tb_clk)
		tb_data_ready = 1; //TO BE SENT BY BUFFER BLOCK
		@(negedge tb_clk)

		if (tb_new_m_data == 1'b1)
			$info ("correctly sent new_m_data signal to encrypt block");
		if (tb_encrypt == 1'b1)
			$info ("correctly enabled encrypt signal to encrypt block");
		@(negedge tb_clk)
		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd1)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//SIMILARYLY SEND 7 MORE PACKETS OF IMAGE DATA

		//PACKET 2
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd2)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 3
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd3)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 4
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd4)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %h", tb_addr_send);

		//PACKET 5
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd5)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 6
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd6)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 7
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd7)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		//PACKET 8
		@(negedge tb_clk)
		@(negedge tb_clk)

		if (tb_e_re == 1)
			$info ("correct enable RE for reading in image data now");
		if (tb_in_data_select == 4'd8)
			$info ("correct buffer register chosen for image data");
		$display ("Address being fetched by master now : = %d", tb_addr_send);

		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		tb_data_encrypted = 1; //SIGNAL FROM ENCRYPTED BLOCK SAYING MESSAGE ENCRYPTED //ACTUAL SIGNAL FROM BLOCK COMES HERE

		@(negedge tb_clk)
		  
		if (tb_new_i_data == 1)
			$info ("correctly sent signal to steganography block to begin steg");

		//WAIT FOR STEGANOGRAPHY TO SEND SIGNAL "STEG DONE"
		@(negedge tb_clk)
		tb_data_steg = 1;
		@(negedge tb_clk)

		if (tb_e_we == 1)
			$info ("PACKET1: correctly received signal to write data (correct sent signal to master)");
		if (tb_out_data_select == 4'd1)
			$info ("Register 1 (of output buffer) correctly chosen");
		if (tb_transfer == 1)
			$info ("correctly received transfer signal (correct sent signal to master)");
		$display ("Address being send to master to write data to : = %d", tb_addr_send);	   

		@(negedge tb_clk)

		if (tb_e_we == 1)
			$info ("PACKET2: correctly received signal to write data (correct sent signal to master)");
		if (tb_out_data_select == 4'd2)
			$info ("Register 2 (of output buffer) correctly chosen");

		if (tb_transfer == 1)
			$info ("correctly received transfer signal (correct sent signal to master)");
		$display ("Address being send to master to write data to : = %d", tb_addr_send);

		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)

		$display ("skipping 5 cycles of writing image data, to last packet of img data");

		@(negedge tb_clk)

		if (tb_e_we == 1)
			$info ("PACKET2: correctly received signal to write data (correct sent signal to master)");
		if (tb_out_data_select == 4'd8)
			$info ("Register 8 (of output buffer) correctly chosen");
		if (tb_transfer == 1)
			$info ("correctly received transfer signal (correct sent signal to master)");
		$display ("Address being send to master to write data to : = %d", tb_addr_send);

		@(negedge tb_clk)
		// NOW THE SAME AS ABOVE HAPPENS 30 MORE TIMES

		@(negedge tb_clk)
		tb_data_ready = 1; //TO BE SENT BY BUFFER BLOCK
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		@(negedge tb_clk)
		tb_data_encrypted = 1;
		@(negedge tb_clk)
		@(negedge tb_clk)
		tb_data_steg = 1;
		tb_start = 0;*/
	end
endmodule
