// $Id: $
// File name:   final_permutation.sv
// Created:     4/16/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  final Design Entry
// Description: Final Permutation table

module final_permutation

  (
   input wire [63:0] final_perm_in,
   output reg [63:0] final_perm_out
   );

   always @ (final_perm_in)
     begin

	final_perm_out[63] = final_perm_in[24];//40
	final_perm_out[62] = final_perm_in[56];//08
	final_perm_out[61] = final_perm_in[16];//48
	final_perm_out[60] = final_perm_in[48];//16
	final_perm_out[59] = final_perm_in[08];//56
	final_perm_out[58] = final_perm_in[40];//24
	final_perm_out[57] = final_perm_in[00];//64
	final_perm_out[56] = final_perm_in[32];//32
	final_perm_out[55] = final_perm_in[25];//29---------------39
	final_perm_out[54] = final_perm_in[57];//07
	final_perm_out[53] = final_perm_in[17];//47
	final_perm_out[52] = final_perm_in[49];//15
	final_perm_out[51] = final_perm_in[09];//55
	final_perm_out[50] = final_perm_in[41];//23
	final_perm_out[49] = final_perm_in[01];//63
	final_perm_out[48] = final_perm_in[33];//31
	final_perm_out[47] = final_perm_in[26];//38
	final_perm_out[46] = final_perm_in[58];//06
	final_perm_out[45] = final_perm_in[18];//46
	final_perm_out[44] = final_perm_in[50];//14
	final_perm_out[43] = final_perm_in[10];//54
	final_perm_out[42] = final_perm_in[42];//22
	final_perm_out[41] = final_perm_in[02];//62
	final_perm_out[40] = final_perm_in[34];//30
	final_perm_out[39] = final_perm_in[27];//37
	final_perm_out[38] = final_perm_in[59];//05
	final_perm_out[37] = final_perm_in[19];//45
	final_perm_out[36] = final_perm_in[51];//13
	final_perm_out[35] = final_perm_in[11];//53
	final_perm_out[34] = final_perm_in[43];//21
	final_perm_out[33] = final_perm_in[03];//61
	final_perm_out[32] = final_perm_in[35];//29
	final_perm_out[31] = final_perm_in[28];//36
	final_perm_out[30] = final_perm_in[60];//04
	final_perm_out[29] = final_perm_in[20];//44
	final_perm_out[28] = final_perm_in[52];//12
	final_perm_out[27] = final_perm_in[12];//52
	final_perm_out[26] = final_perm_in[44];//20
	final_perm_out[25] = final_perm_in[04];//60
	final_perm_out[24] = final_perm_in[36];//28
	final_perm_out[23] = final_perm_in[29];//35
	final_perm_out[22] = final_perm_in[61];//03
	final_perm_out[21] = final_perm_in[21];//43
	final_perm_out[20] = final_perm_in[53];//11
	final_perm_out[19] = final_perm_in[13];//51
	final_perm_out[18] = final_perm_in[45];//19
	final_perm_out[17] = final_perm_in[05];//59
	final_perm_out[16] = final_perm_in[37];//27
	final_perm_out[15] = final_perm_in[30];//34
	final_perm_out[14] = final_perm_in[62];//02
	final_perm_out[13] = final_perm_in[22];//42
	final_perm_out[12] = final_perm_in[54];//10
	final_perm_out[11] = final_perm_in[14];//50
	final_perm_out[10] = final_perm_in[46];//18
	final_perm_out[09] = final_perm_in[06];//58
	final_perm_out[08] = final_perm_in[38];//26
	final_perm_out[07] = final_perm_in[31];//33
	final_perm_out[06] = final_perm_in[63];//01
	final_perm_out[05] = final_perm_in[23];//41
	final_perm_out[04] = final_perm_in[55];//09
	final_perm_out[03] = final_perm_in[15];//49
	final_perm_out[02] = final_perm_in[47];//17
	final_perm_out[01] = final_perm_in[07];//57
	final_perm_out[00] = final_perm_in[39];//25

     end // always @ (final_perm_in)
endmodule // final_permutation
