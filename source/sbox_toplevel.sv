// $Id: $
// File name:   sbox_toplevel.sv
// Created:     4/20/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: coordinates sboxs

module sbox_toplevel
  (
   input wire [47:0] in,
   output reg [31:0] out
   );

   sbox1 sbox1(
	       .in(in[47:42]),
	       .out(out[31:28])
	       );
   sbox2 sbox2(
	       .in(in[41:36]),
	       .out(out[27:24])
	       );
   sbox3 sbox3(
	       .in(in[35:30]),
	       .out(out[23:20])
	       );
   sbox4 sbox4(
	       .in(in[29:24]),
	       .out(out[19:16])
	       );
   sbox5 sbox5(
	       .in(in[23:18]),
	       .out(out[15:12])
	       );
   sbox6 sbox6(
	       .in(in[17:12]),
	       .out(out[11:08])
	       );
   sbox7 sbox7(
	       .in(in[11:6]),
	       .out(out[07:04])
	       );
   sbox8 sbox8(
	       .in(in[5:0]),
	       .out(out[3:0])
	       );
endmodule // sbox_toplevel
