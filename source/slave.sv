// $Id: $
// File name:   slave.sv
// Created:     4/12/2016
// Author:      Anirudh Ghantasala
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: slave for the finalproj program that communicates with cpu


module slave
(
	input reg 			clk,
	input reg 			n_rst,
	input reg 			hsel1,
	input reg 			hwrite,
	input reg [2:0] 	hburst,
	input reg [3:0] 	hprot,
	input reg [2:0] 	hsize,
	input reg [1:0] 	htrans,
	input reg 			hmastlock2,
	input reg 			hready,
	input reg [18:0] 	haddr,		// tells the slave where to store the data coming in in the next cycle
	input reg [63:0]  hwdata,
	
	output reg 			hreadyout,
	output reg 			hresp,	
	output reg 			start,
	output reg 			enorde,
	output reg [18:0] start_addr,
	output reg [191:0] enc_key
	

);

reg [2:0] cur_store, next_store; 
reg n_start, n_enorde; 
reg [18:0] n_start_addr;
reg [191:0] n_enc_key;

always_ff @ (posedge clk, negedge n_rst)
	begin
		if(~n_rst)
		begin
			cur_store <= 0;
			start <= 0;
			enorde <= 0;
			start_addr <= 0;
			enc_key <= 0;
			
		end
		else
		begin
			cur_store <= next_store;
			start <= n_start;
			enorde <= n_enorde;
			start_addr <= n_start_addr;
			enc_key <= n_enc_key;	
		end
	end
always_comb
	begin
		n_start = start;
		n_enorde = enorde;
		n_start_addr = start_addr;
		n_enc_key = enc_key;
		next_store = haddr;

		if(cur_store != 0)// address phase has just started
		begin		
			case(cur_store)
				3'd1: n_start = hwdata[0];
				3'd2: n_enorde = hwdata[0];
				3'd3: n_start_addr = hwdata[18:0];
				3'd4: n_enc_key[63:0] = hwdata;
				3'd5:	n_enc_key[127:64] = hwdata;
				3'd6: n_enc_key[191:128] = hwdata;
			endcase
		end
	end
endmodule
