// $Id: $
// File name:   tb_sbox.sv
// Created:     4/16/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Initial Permutation Test Bench

`timescale 1ns/100ps


module tb_initial_permutation
  ();   
   wire [63:0]tb_in;
   wire [63:0]tb_out;
   reg [63:0] tb_test_inputs;
   
   assign tb_in = tb_test_inputs;
   
   
   initial_permutation DUT(
	    .initial_perm_in(tb_in),
	    .initial_perm_out(tb_out)
	    );   
   initial begin
      tb_test_inputs[63:0] = 64'b0000000100100011010001010110011110001001101010111100110111101111;
      

   end
endmodule // tb_sbox
