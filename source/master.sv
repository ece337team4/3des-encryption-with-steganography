// $Id: $
// File name:   master.sv
// Created:     4/12/2016
// Author:      Anirudh Ghantasala
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: Master for the final project encrypt-steg program

module master(
	input reg 			HRESP,
	input reg 			HREADY,
	input reg 			e_re,
	input reg 			e_we,
	input reg  [18:0]	send_addr,
	input reg 			transfer,
	input reg  [63:0]	data_in,
	input reg  [63:0]	HRDATA,
	output reg [63:0]	HWDATA,
	output reg [2:0]	HSIZE,
	output reg 			HWRITE,
	output reg [2:0]	HBURST,
	output reg [3:0]	HPROT,
	output reg [18:0]	HADDR,
	output reg 			HMASTLOCK,
	output reg [1:0]	HTRANS,
	output reg 			controller_hresp,		// wait a clock cycle if hresp is 1 (slave needs more time)
	output reg [63:0] data_out,
	output reg 			transfer_complete
);
	

	always_comb 
	begin
		// set HWRITE		
		if(~e_re && e_we)
			HWRITE = 1'b1;
		else 				// if not write, default to read status
			HWRITE = 1'b0;

	   
	
	end	
	
        assign HTRANS = 2'b00;
	assign HPROT = 4'b0001;
	assign HBURST = 3'b000;
	assign HSIZE = 3'b011;
	assign HMASTBLOCK = 1'b0;
	assign HWDATA = data_in;
	assign data_out = HRDATA;
//	assign HRESP = controller_hresp;
	assign transfer_complete = HREADY;
    assign RE = e_re;
    assign WE = e_we;
   assign HADDR = send_addr;
   
   
endmodule


