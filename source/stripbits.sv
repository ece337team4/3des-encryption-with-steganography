// $Id: $
// File name:   stripbits.sv
// Created:     4/23/2016
// Author:      Akshay Sanjeev Daftardar
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: strips bits from image
module stripbits
(
	input wire [511:0] image,
	output reg [63:0] message
);

always_comb
begin
		message[0] = image[0];
		message[1] = image[8];
		message[2] = image[16];
		message[3] = image[24];
		message[4] = image[32];
		message[5] = image[40];
		message[6] = image[48];
		message[7] = image[56];
		message[8] = image[64];
		message[9] = image[72];
		message[10] = image[80];
		message[11] = image[88];
		message[12] = image[96];
		message[13] = image[104];
		message[14] = image[112];
		message[15] = image[120];
		message[16] = image[128];
		message[17] = image[136];
		message[18] = image[144];
		message[19] = image[152];
		message[20] = image[160];
		message[21] = image[168];
		message[22] = image[176];
		message[23] = image[184];
		message[24] = image[192];
		message[25] = image[200];
		message[26] = image[208];
		message[27] = image[216];
		message[28] = image[224];
		message[29] = image[232];
		message[30] = image[240];
		message[31] = image[248];
		message[32] = image[256];
		message[33] = image[264];
		message[34] = image[272];
		message[35] = image[280];
		message[36] = image[288];
		message[37] = image[296];
		message[38] = image[304];
		message[39] = image[312];
		message[40] = image[320];
		message[41] = image[328];
		message[42] = image[336];
		message[43] = image[344];
		message[44] = image[352];
		message[45] = image[360];
		message[46] = image[368];
		message[47] = image[376];
		message[48] = image[384];
		message[49] = image[392];
		message[50] = image[400];
		message[51] = image[408];
		message[52] = image[416];
		message[53] = image[424];
		message[54] = image[432];
		message[55] = image[440];
		message[56] = image[448];
		message[57] = image[456];
		message[58] = image[464];
		message[59] = image[472];
		message[60] = image[480];
		message[61] = image[488];
		message[62] = image[496];
		message[63] = image[504];
end

endmodule 
