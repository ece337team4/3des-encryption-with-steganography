// $Id: $
// File name:   steganography.sv
// Created:     4/16/2016
// Author:      Anirudh Ghantasala, Akshay Daftardar
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: steganography block in which the message is hid in the image


module steganography
(		
		input reg [63:0] encrypt_m,
		input reg [511:0] data_i,
		output reg [511:0] inter
);

integer i;
reg [8:0] m;
always_comb
begin
	for(i = 0; i < 512; i++)
	begin
		m = i / 8;
		if (i % 8 == 0)
		begin
			inter[i] = encrypt_m[m];
		end
		else
		begin
			inter[i] = data_i[i];
		end
	end
end
// SET STEG_IMG AND DATA_STEG, only when new_i_data == 1
endmodule

