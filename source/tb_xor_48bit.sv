// $Id: $
// File name:   tb_xor_48bit.sv
// Created:     4/18/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Test bench for DES function XOR with round key

`timescale 10ns/100ps

module tb_xor_48bit
  ();

   wire [47:0] tb_plaintext_in;
   wire [47:0] tb_roundkey_in;
   wire [47:0] tb_xor_out;
   reg [47:0]  tb_test_plaintext;
   reg [47:0]  tb_test_roundkey;

   assign tb_plaintext_in = tb_test_plaintext;
   assign tb_roundkey_in = tb_test_roundkey;

   xor_48bit DUT (
		.plaintext_in(tb_plaintext_in),
		.roundkey_in(tb_roundkey_in),
		.xor_out(tb_xor_out)
		);
   initial begin
      
      tb_test_plaintext = 48'b011110100001010101010101011110100001010101010101;
      tb_test_roundkey = 48'b000110110000001011101111111111000111000001110010;
   
   end
endmodule // tb_xor_48_bit

