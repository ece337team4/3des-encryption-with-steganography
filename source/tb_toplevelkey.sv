// $Id: $
// File name:   tb_toplevelkey.sv
// Created:     4/17/2016
// Author:      Akshay Sanjeev Daftardar
// Lab Section: 337-01
// Version:     1.0  Initial Design Entry
// Description: test bench to test the key generation
module tb_toplevelkey();
	timeunit 1ns;
	reg clk;
	localparam CLK_PERIOD = 10ns;
	reg n_rst;
	reg [63:0] key;	
	reg [47:0] roundkey;
	reg tb_sendready;
	reg tb_encrypt;
	always
	begin
		clk = 1;
		#(CLK_PERIOD / 2.0);
		clk = 0;
		#(CLK_PERIOD/ 2.0);
	end
	toplevelkey KEYFUL(.clk(clk), .n_rst(n_rst), .key(key), .encrypt(tb_encrypt),.sendready(tb_sendready), .roundkey(roundkey));
	initial
	begin
		//ROUND 0
		n_rst = 0;
		tb_encrypt = 0;
		tb_sendready = 0;
		key = 64'hAABB09182736CCDD;	
		@(posedge clk);
		@(negedge clk);
		n_rst = 1;
		@(negedge clk);
		tb_encrypt = 1;
		key = 64'hAABB09182736CCDD;
		tb_sendready = 0;
		@(negedge clk);
		tb_sendready = 1;
		/*//ROUND 1
		@(negedge clk);
		tb_sendready = 0;
		//ROUND 2
		@(negedge clk);
		tb_sendready = 1;
		//ROUND 3
		@(negedge clk);
		tb_sendready = 0;
		//ROUND 4
		@(negedge clk);
		tb_sendready = 1;
		//ROUND 5
		@(negedge clk);
		tb_sendready = 0;
		//ROUND 6
		@(negedge clk);
		tb_sendready = 1;
		//ROUND 7
		@(negedge clk);
		tb_sendready = 0;
		//ROUND 8
		@(negedge clk);
		tb_sendready = 1;
		//ROUND 9
		@(negedge clk);
		tb_sendready = 0;
		//ROUND 10
		@(negedge clk);
		tb_sendready = 1;
		//ROUND 11
		@(negedge clk);
		tb_sendready = 0;
		//ROUND 12
		@(negedge clk);
		tb_sendready = 1;
		//ROUND 13
		@(negedge clk);
		tb_sendready = 0;
		//ROUND 14
		@(negedge clk);
		tb_sendready = 1;
		//ROUND 15*/
	end
endmodule
		
		