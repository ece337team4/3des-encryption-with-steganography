// $Id: $
// File name:   swapper.sv
// Created:     4/19/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: swaps left and right 32 bits

module swapper
  (
   input wire noswap,
   input wire [63:0] in,
   output reg [63:0] out
   );

   always @ (in)
     begin
	if (noswap == 1) begin
	   out = in;
	end
	else begin
	   out[63:32] = in[31:00];
	   out[31:00] = in[63:32];
	end
     end
endmodule // swapper

   