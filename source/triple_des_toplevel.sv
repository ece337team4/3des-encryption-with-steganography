// $Id: $
// File name:   triple_des_toplevel.sv
// Created:     4/24/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: Encryption functional block

module triple_des_toplevel
  (
   input wire 	      clk,
   input wire 	      n_rst,
   input wire 	      new_m_data,
   input wire 	      encrypt,
   input wire [191:0] complete_key,
   input reg [63:0]   m_data,
	input reg [63:0]   stripped_m_data,
   output reg [63:0]  cipher_out,
	output reg [63:0]  cipher_out_decrypt,
   //output reg encrypt_ctrl,
   output reg 	      data_encrypted
   //output reg [1:0] stagenum
   );

   reg [63:0] 	      stage1, n_stage1;
   reg 		      start;
   reg 		      stage_done;
   reg [63:0] 	      key_part;
   reg [63:0] 	      stage2;
   //reg [63:0] 	      initialin;
   reg [1:0] 	      stagenum;
   reg [63:0] 	      n_cipher;
   reg 		      encrypt_ctrl;
   


   
   encryption_toplevel des1(
			    .plain_text(stage1),
			    .cipher_text(stage2),
			    .clk(clk),
			    .n_rst(n_rst),
			    .full_key(key_part),
			    .start(start),
			    .encrypt(encrypt_ctrl),
			    .stage_done(stage_done),
			    .stagenum(stagenum)
			    );
   
   typedef enum       bit [4:0] {idle, encryptL, encryptM, encryptR, decryptL, decryptM, decryptR, delay1, delay2, delay3, delay4, delay5, delay6, delay7,delay8, delay9, delay10, delay11, pulse} state_type;

   state_type c_state, n_state;

   always_ff @ (posedge clk, negedge n_rst)begin
      if(n_rst == 0) begin
	 c_state <= idle;
	 stage1 <= 0;
	 cipher_out <= 0;
      end
      else begin
	 stage1 <= n_stage1;
	 c_state <= n_state;
	 cipher_out <= n_cipher;
	 
      end
   end

   always_comb
     begin
	cipher_out_decrypt = cipher_out;
	n_state = c_state;
	n_stage1 = stage1;
	stagenum = 2'd0;
	n_cipher = cipher_out;
	key_part = 64'b0;
	encrypt_ctrl = 0;
	data_encrypted = 0;
	start = 0;
	case(c_state)
	  idle: begin
	     if(new_m_data == 1 && encrypt == 1) begin
		n_stage1 = m_data;
		
		n_state = encryptL;
	     end 
	     else if (new_m_data == 1 && encrypt == 0) begin
		n_state = decryptL;
		n_stage1 = stripped_m_data;
		
	     end
	     else
	       begin
		  n_stage1 = 64'b0;
		  stagenum = 0;
		  data_encrypted = 0;
		  n_state = idle;
		  n_cipher = 0;
		  
	       end
	  end

	  encryptL: begin
	     key_part = complete_key[191:128];
	     if(stage_done == 1)begin
		n_stage1 = stage2;
		n_state = decryptM;
	     end else begin
		n_state = encryptL;
		n_stage1 = m_data;
	     end
	     start = 1; 
	     stagenum = 2'd1;
	     data_encrypted = 0;
	     encrypt_ctrl = 1;
	     n_cipher = 0;
	     
	  end

	  decryptM: begin
	     key_part = complete_key[127:64];
	     if(stage_done == 1)begin
		n_stage1 = stage2;	
		n_state = encryptR;
	     end else begin
		n_state = decryptM;
		n_stage1 = stage1;	
	     end
	     start = 1;
	     data_encrypted = 0;
	     encrypt_ctrl = 0;
	     stagenum = 2'd2;
	     n_cipher = 0;
	     
	  end

	  encryptR: begin
	     key_part = complete_key[63:0];
	     if(stage_done == 1)begin
		//n_state = pulse;
		n_state = delay1;
		n_stage1 = stage2;
		n_cipher = stage2;
		
		//data_encrypted = 1;
	     end else begin
		n_state = encryptR;
		n_stage1 = stage1;
		n_cipher = 0;
		
	     end
	     start = 1;
	     encrypt_ctrl = 1;
	     stagenum = 2'd3;
	     data_encrypted = 0;
	     
	     end // case: encryptR
	  delay1: begin
	     n_state = delay2;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end
	  delay2: begin
	     n_state = delay3;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end	  
	  delay3: begin
	     n_state = delay4;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end	  
	  delay4: begin
	     n_state = delay5;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end	  
	  delay5: begin
	     n_state = delay6;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end	  
	  delay6: begin
	     n_state = delay7;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end	  
	  delay7: begin
	     n_state = delay8;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end
	  delay8: begin
	     n_state = delay9;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end
	  delay9: begin
	     n_state = delay10;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end	  
	  delay10: begin
	     n_state = delay11;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end	  
	  delay11: begin
	     n_state = idle;
	     n_stage1 = cipher_out;
	     n_cipher = cipher_out;
	     start = 0;
	     encrypt_ctrl = 0;
	     stagenum = 0;
	     data_encrypted = 1;
	  end
	  decryptL: begin
	     key_part = complete_key[191:128];
	     if(stage_done == 1) begin
		n_stage1 = stage2;
		n_state = encryptM;
	     end else begin
		n_state = decryptL;
		n_stage1 = stripped_m_data;
	     end
	     start = 1;
	     encrypt_ctrl = 0;
	     data_encrypted = 0; 
	     stagenum = 2'd1;
	     n_cipher = 0;
	     
	     
	  end
	  
	  encryptM: begin
	     key_part = complete_key[127:64];
	     if(stage_done == 1)begin
		n_state = decryptR;
		n_stage1 = stage2;
		
	     end else begin
		n_state = encryptM;
		n_stage1 = stage1;
		
	     end
	     start = 1;
	     
	     encrypt_ctrl = 1;
	     data_encrypted = 0;
	     
	     stagenum = 2'd2;
	     n_cipher = 0;
	     
	     
	  end
	  
	  decryptR: begin
	     key_part = complete_key[63:0];
	     if(stage_done == 1)begin
		//n_state = pulse;
		n_state = delay1;
		n_cipher = stage2;
		n_stage1 = stage2;
	     end else begin
		n_state = decryptR;
		n_stage1 = stage1;
	     end
	     start = 1;
	     encrypt_ctrl = 0;
	     stagenum = 2'd3;
	     data_encrypted = 0;
	     
	  end
	  pulse:
	    begin
	       data_encrypted = 1;
	       n_state = idle;
	       stagenum = 2'd3;
	       
	       //n_cipher = stage2;
	       
	       
	    end
	  
	endcase // case (c_state)
	
     end
     
   //assign finalout = (c_state == decryptR ) ?  
   //assign data_in = (c_state == encryptL || c_state == decryptL) ? m_data :
   //(c_state == idle) ? 64'b0 : stage1;
endmodule // triple_des_toplevel
