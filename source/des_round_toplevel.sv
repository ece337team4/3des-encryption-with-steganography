// $Id: $
// File name:   des_round_toplevel.sv
// Created:     4/19/2016
// Author:      Trevor Bonesteel
// Lab Section: 337-02
// Version:     1.0  Initial Design Entry
// Description: coordinates encryption module for one round

module des_round_toplevel
  (
   input wire noswap,
   input wire [63:0] data_msg,
   input wire [47:0] round_key,
   output reg [63:0] cipher_out

   );
   reg [31:0] 	     left_xored;
   reg [47:0] 	     expanded_bits;
   reg [47:0] 	     right_keyed;
   reg [31:0] 	     right_sboxed;
   reg [31:0] 	     right_dboxed;
   reg [31:0] 	     right_bits;
   reg [31:0] 	     left_bits;
   

   split_bits split(
		    .plaintext(data_msg),
		    .bitsL(left_bits),
		    .bitsR(right_bits)
		    );
   
   xor_32bit xor1(
		 .left_in(left_bits),
		 .right_in(right_dboxed),
		 .xor_out(left_xored)
		 );
   straight_dbox straight_dbox(
		     .in(right_sboxed),
		     .out(right_dboxed)
		     );

   sbox_toplevel sbox(
	    .in(right_keyed),
	    .out(right_sboxed)
	    );

   expansion_dbox expansion_dbox(
		      .expansion_in(right_bits),
		      .expansion_out(expanded_bits)
		      );
   swapper swapper(
	       .noswap(noswap),
	       .in({left_xored,data_msg[31:0]}),
	       .out(cipher_out)
	       );
   xor_48bit xor2(
		 .plaintext_in(expanded_bits),
		 .roundkey_in(round_key),
		 .xor_out(right_keyed)
		 );

endmodule // des_round_toplevel

   
